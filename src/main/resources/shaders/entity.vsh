#version 430

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 textureCoordinate;
layout(location = 2) in vec3 normal;

out vec2 fragmentTextureCoordinate;
out vec3 fragmentNormal;
out vec3 fragmentToLight;
out vec3 fragmentToCamera;

uniform vec3 lightPosition;
uniform mat4 perspectiveMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

void main(){
    vec3 worldPosition = (modelMatrix * vec4(position, 1.0)).xyz;

    fragmentNormal = (modelMatrix * vec4(normal, 1.0)).xyz;
    fragmentToLight = lightPosition-worldPosition;
    fragmentToCamera = (inverse(viewMatrix) * vec4(0.0,0.0,0.0,1.0)).xyz - worldPosition;

    gl_Position = perspectiveMatrix * viewMatrix * vec4(worldPosition, 1.0);
    fragmentTextureCoordinate = textureCoordinate;
}