#version 430

layout (location = 0) in vec2 position;

out vec2 fragmentTextureCoordinate;

uniform mat4 perspectiveMatrix;
uniform mat4 modelMatrix;

void main() {
    gl_Position = perspectiveMatrix * modelMatrix * vec4(position, 0.0, 1.0);
    fragmentTextureCoordinate = vec2((position.x + 1.0) / 2.0, (position.y + 1.0) / 2.0);
}