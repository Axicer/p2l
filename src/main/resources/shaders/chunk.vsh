#version 430

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

out vec2 fragmentTextureCoordinates;
out vec3 fragmentToLight;
out vec3 fragmentNormal;

uniform vec3 lightPosition;
uniform mat4 perspectiveMatrix;
uniform mat4 viewMatrix;

void main(){
    fragmentNormal = normal;
    fragmentToLight = lightPosition-position;
    fragmentTextureCoordinates = texCoord;

    gl_Position = perspectiveMatrix * viewMatrix * vec4(position, 1.0);
}