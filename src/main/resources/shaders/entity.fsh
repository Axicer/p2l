#version 430

in vec2 fragmentTextureCoordinate;
in vec3 fragmentNormal;
in vec3 fragmentToLight;
in vec3 fragmentToCamera;

out vec4 fragmentColor;

uniform sampler2D textureSampler;
uniform vec4 lightColor;
uniform float reflectivity;
uniform float shineDamper;

void main(){
    vec3 unitNormal = normalize(fragmentNormal);
    vec3 unitToLight = normalize(fragmentToLight);

    float normalDotToLight = dot(unitNormal, unitToLight);
    float brightness = max(normalDotToLight, 0.1);
    vec3 diffuse = brightness * lightColor.xyz;

    vec3 unitToCamera = normalize(fragmentToCamera);
    vec3 lightDirection = -unitToLight;
    vec3 reflectLightDirection = reflect(lightDirection, unitNormal);

    float specularFactor = dot(reflectLightDirection, unitToCamera);
    specularFactor = max(specularFactor, 0.0);
    float dampedFactor = pow(specularFactor, shineDamper);
    vec3 finalSpecular = dampedFactor * reflectivity * lightColor.xyz;

    fragmentColor = vec4(diffuse, 1.0) * texture(textureSampler, fragmentTextureCoordinate) + vec4(finalSpecular, 1.0);
}