#version 430

in vec2 fragmentTextureCoordinates;
in vec3 fragmentToLight;
in vec3 fragmentNormal;

out vec4 fragColor;

uniform vec4 lightColor;
uniform sampler2D atlasSampler;

void main(){
    vec3 unitNormal = normalize(fragmentNormal);
    vec3 unitToLight = normalize(fragmentToLight);

    float normalDotToLight = dot(unitNormal, unitToLight);
    float brightness = max(normalDotToLight, 0.5);
    vec3 diffuse = brightness * lightColor.xyz;

    fragColor = vec4(diffuse, 1.0) * lightColor * texture(atlasSampler, fragmentTextureCoordinates);
}