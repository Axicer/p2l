#version 430

in vec2 fragmentTextureCoordinates;

out vec4 fragColor;

uniform vec4 color;
uniform sampler2D textureSampler;

void main(){
    fragColor = color * texture(textureSampler, fragmentTextureCoordinates);
}