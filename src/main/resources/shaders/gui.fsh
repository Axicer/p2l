#version 430

in vec2 fragmentTextureCoordinate;

out vec4 fragmentColor;

uniform vec4 color;
uniform sampler2D textureSampler;

void main(){
    fragmentColor = color * texture(textureSampler, fragmentTextureCoordinate);
}