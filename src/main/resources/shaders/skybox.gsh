#version 330

layout ( points ) in;
layout ( triangle_strip, max_vertices = 36 ) out;

out vec3 texCoords;

uniform float cubeSize;
uniform mat4 viewMatrix;
uniform mat4 perspectiveMatrix;

void createVertex(vec3 offset){
    vec4 worldPosition = gl_in[0].gl_Position + vec4(offset.x * cubeSize, offset.y * cubeSize, offset.z * cubeSize, 0.0);
    gl_Position = perspectiveMatrix * viewMatrix * worldPosition;

    texCoords = offset;

    EmitVertex();
}

void main(){

    //BACK
    createVertex(vec3(-1.0, 1.0, -1.0));
    createVertex(vec3(-1.0, -1.0, -1.0));
    createVertex(vec3(1.0, -1.0, -1.0));
    createVertex(vec3(1.0, -1.0, -1.0));
    createVertex(vec3(1.0, 1.0, -1.0));
    createVertex(vec3(-1.0, 1.0, -1.0));

    //RIGHT
    createVertex(vec3(1.0, -1.0, -1.0));
    createVertex(vec3(1.0, -1.0, 1.0));
    createVertex(vec3(1.0, 1.0, -1.0));
    createVertex(vec3(1.0, 1.0, -1.0));
    createVertex(vec3(1.0, 1.0, 1.0));
    createVertex(vec3(1.0, -1.0, 1.0));

    //FRONT
    createVertex(vec3(-1.0, 1.0, 1.0));
    createVertex(vec3(1.0, -1.0, 1.0));
    createVertex(vec3(1.0, 1.0, 1.0));
    createVertex(vec3(-1.0, 1.0, 1.0));
    createVertex(vec3(-1.0, -1.0, 1.0));
    createVertex(vec3(1.0, -1.0, 1.0));

    //LEFT
    createVertex(vec3(-1.0, -1.0, 1.0));
    createVertex(vec3(-1.0, 1.0, 1.0));
    createVertex(vec3(-1.0, 1.0, -1.0));
    createVertex(vec3(-1.0, -1.0, 1.0));
    createVertex(vec3(-1.0, -1.0, -1.0));
    createVertex(vec3(-1.0, 1.0, -1.0));

    //TOP
    createVertex(vec3(-1.0, 1.0, 1.0));
    createVertex(vec3(-1.0, 1.0, -1.0));
    createVertex(vec3(1.0, 1.0, -1.0));
    createVertex(vec3(1.0, 1.0, 1.0));
    createVertex(vec3(-1.0, 1.0, 1.0));
    createVertex(vec3(1.0, 1.0, -1.0));

    //BOTTOM
    createVertex(vec3(1.0, -1.0, -1.0));
    createVertex(vec3(1.0, -1.0, 1.0));
    createVertex(vec3(-1.0, -1.0, 1.0));
    createVertex(vec3(1.0, -1.0, -1.0));
    createVertex(vec3(-1.0, -1.0, 1.0));
    createVertex(vec3(-1.0, -1.0, -1.0));

    EndPrimitive();
}