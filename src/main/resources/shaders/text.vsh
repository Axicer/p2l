#version 430

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 texcoord;

out vec2 fragmentTextureCoordinates;

uniform mat4 perspectiveMatrix;

void main(){
    fragmentTextureCoordinates = texcoord;

    gl_Position = perspectiveMatrix * vec4(position.x, position.y, 0.0, 1.0);
}