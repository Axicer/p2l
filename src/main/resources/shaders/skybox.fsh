#version 430

in vec3 texCoords;
out vec4 fragColor;

uniform samplerCube cubeImage;
uniform vec4 color;

void main(){
    fragColor = color * texture(cubeImage, vec3(-texCoords.x, -texCoords.y, texCoords.z));
}