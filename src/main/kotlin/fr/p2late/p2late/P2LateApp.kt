package fr.p2late.p2late

import fr.p2late.p2late.client.P2LClient
import fr.p2late.p2late.server.P2LServer
import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.default

object App {
    const val appName = "P2Late"
    const val version = "0.0.1"
}

fun main(args: Array<String>) {
    val parser = ArgParser("${App.appName}:: ${App.version}")
    val version by parser.option(ArgType.Boolean, shortName = "v", description = "Show version").default(false)
    val server by parser.option(ArgType.Boolean, shortName = "s", description = "Run Server").default(false)
    parser.parse(args)

    if(version){
        println(App.version)
        return
    }

    if(server){
        P2LServer()
    }else{
        P2LClient.start()
    }
}