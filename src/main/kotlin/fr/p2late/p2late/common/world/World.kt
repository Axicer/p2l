package fr.p2late.p2late.common.world

import fr.p2late.p2late.common.blocks.Block
import fr.p2late.p2late.common.blocks.BlockType
import fr.p2late.p2late.common.math.OpenSimplexNoise
import kotlin.math.abs
import kotlin.math.floor

class World(
    private val seed: Long = 0L,
    private val oceanLevel: Int = 70,
    private val attenuation: Float = 15f,
    private val amplitude: Float = 10f
) {
    private var terrainNoise = OpenSimplexNoise(seed)
    val loadedChunk: MutableMap<Pair<Int, Int>, Chunk> = mutableMapOf()

    fun getChunk(x: Int, z: Int): Chunk? = loadedChunk[x to z]

    fun setChunk(x: Int, z: Int, chunk: Chunk) {
        chunk.x = x
        chunk.z = z
        loadedChunk[x to z] = chunk
    }

    fun generateChunk(chunkX: Int, chunkZ: Int): Chunk {
        val chunk = Chunk(this)
//        setChunk(chunkX, chunkZ, chunk)
//        setBlock(0, 0, 0, Block(BlockType.BEDROCK))

        for (x in 0 until Chunk.SIZE_X) {
            for (z in 0 until Chunk.SIZE_Z) {
                val noise = (terrainNoise.random2D(
                    (chunkX * Chunk.SIZE_X + x.toDouble()) / attenuation,
                    (chunkZ * Chunk.SIZE_Z + z.toDouble()) / attenuation
                ) + 1) / 2
                val terrainLevel = oceanLevel + (noise * amplitude).toInt()
                for (y in Chunk.MIN_Y until terrainLevel) {
                    val type =
                        if (y > terrainLevel - 5) BlockType.DIRT
                        else when (y) {
                            -64 -> BlockType.BEDROCK
                            -63 -> if(Math.random() > 0.2) BlockType.BEDROCK else BlockType.DEEPSTONE
                            -62 -> if(Math.random() > 0.4) BlockType.BEDROCK else BlockType.DEEPSTONE
                            -61 -> if(Math.random() > 0.6) BlockType.BEDROCK else BlockType.DEEPSTONE
                            -60 -> if(Math.random() > 0.8) BlockType.BEDROCK else BlockType.DEEPSTONE
                            in -59 until 0 -> BlockType.DEEPSTONE
                            0 -> if(Math.random() > 0.2) BlockType.DEEPSTONE else BlockType.STONE
                            1 -> if(Math.random() > 0.4) BlockType.DEEPSTONE else BlockType.STONE
                            2 -> if(Math.random() > 0.6) BlockType.DEEPSTONE else BlockType.STONE
                            3 -> if(Math.random() > 0.8) BlockType.DEEPSTONE else BlockType.STONE
                            else -> BlockType.STONE
                        }
                    chunk.setBlock(x, y - Chunk.MIN_Y, z, Block(type))
                }
            }
        }
        setChunk(chunkX, chunkZ, chunk)

        return chunk
    }

    //world coordinates
    fun getBlock(x: Int, y: Int, z: Int): Block? {
        checkPositionRequirement(x, y, z)
        val chunkX = floor((x.toDouble() / Chunk.SIZE_X)).toInt()
        val chunkZ = floor((z.toDouble() / Chunk.SIZE_Z)).toInt()

        val blockX = x % Chunk.SIZE_X
        val blockZ = z % Chunk.SIZE_Z

        return getChunk(chunkX, chunkZ)?.getBlock(blockX, y + Chunk.MIN_Y, blockZ)
    }

    //world coordinates
    fun setBlock(x: Int, y: Int, z: Int, block: Block, forceGeneration: Boolean = false) {
        checkPositionRequirement(x, y, z)
        val chunkX = floor((x.toDouble() / Chunk.SIZE_X)).toInt()
        val chunkZ = floor((z.toDouble() / Chunk.SIZE_Z)).toInt()
        val chunk = getChunk(chunkX, chunkZ) ?: if (forceGeneration) generateChunk(chunkX, chunkZ) else null
        val blockX = x % Chunk.SIZE_X
        val blockZ = z % Chunk.SIZE_Z
        chunk?.setBlock(blockX, y + abs(Chunk.MIN_Y), blockZ, block)
    }

    private fun checkPositionRequirement(x: Int, y: Int, z: Int) {
        require(y in Chunk.MIN_Y until Chunk.MAX_Y) { "Y coordinate should be between ${Chunk.MIN_Y} (inclusive) and ${Chunk.MAX_Y} (exclusive) !" }
    }
}