package fr.p2late.p2late.common.world

import fr.p2late.p2late.common.blocks.Block

class Chunk(
    val world: World
) {
    var x = 0
    var z = 0

    companion object {
        const val SIZE_X = 16
        const val MIN_Y = -64
        const val MAX_Y = 320
        const val SIZE_Z = 16
    }

    private var blocks: Array<Array<Array<Block?>>> =
        Array(SIZE_X) { Array(MAX_Y - MIN_Y) { Array(SIZE_Z) { null } } }

    private fun checkPosRequirement(x: Int, y: Int, z: Int) {
        require(x in 0 until SIZE_X) { "x value must be between 0 (inclusive) and $SIZE_X (exclusive) -> given $x" }
        require(y in 0 until (MAX_Y - MIN_Y)) { "y value must be between 0 (inclusive) and ${MAX_Y - MIN_Y} (exclusive) -> given $y" }
        require(z in 0 until SIZE_Z) { "z value must be between 0 (inclusive) and $SIZE_Z (exclusive)  -> given $z" }
    }

    //coordinates as chunk
    fun getBlock(x: Int, y: Int, z: Int): Block? {
        checkPosRequirement(x, y, z)
        return blocks[x][y][z]
    }

    //coordinate as chunk
    fun setBlock(x: Int, y: Int, z: Int, block: Block) {
        checkPosRequirement(x, y, z)
        block.chunk = this
        block.x = x
        block.y = y
        block.z = z
        blocks[x][y][z] = block
    }
}