package fr.p2late.p2late.common

import org.lwjgl.BufferUtils
import java.io.IOException
import java.nio.ByteBuffer

fun getResourceAsString(path: String) = getResourceAsInputStream(path).bufferedReader().readText()
fun getResourceAsText(path: String) = getResourceAsInputStream(path).bufferedReader().readLines()
fun getResourceAsByteBuffer(path: String): ByteBuffer = getResourceAsInputStream(path).use {
    val bytes = it.readAllBytes()
    val buffer = BufferUtils.createByteBuffer(bytes.size)
    buffer.put(bytes).flip()
}
fun getResourceAsInputStream(path: String) =
    object {}.javaClass.getResourceAsStream(path) ?: throw IOException("Failed to load resource $path !")