package fr.p2late.p2late.common

import org.lwjgl.glfw.GLFW

fun Boolean.toGLFWBoolean() = if(this) GLFW.GLFW_TRUE else GLFW.GLFW_FALSE