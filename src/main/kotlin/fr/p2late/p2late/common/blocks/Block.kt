package fr.p2late.p2late.common.blocks

import fr.p2late.p2late.common.world.Chunk

data class Block(
    val type: BlockType,
    val attributes: MutableList<BlockAttributes> = mutableListOf()
) {
    lateinit var chunk: Chunk

    /**
     * coordinates as in chunk ref
     */
    var x = 0
    var y = 0
    var z = 0

    /**
     * coordinates as in world ref
     */
    fun getWorldX() = Chunk.SIZE_X * chunk.x + x
    fun getWorldY() = y + Chunk.MIN_Y
    fun getWorldZ() = Chunk.SIZE_Z * chunk.z + z

    fun getAdjacentBlock(face: BlockFace): Block? {
        var adjacentX = x + face.relativeX
        var adjacentY = y + face.relativeY
        var adjacentZ = z + face.relativeZ

        if (adjacentY !in 0 until (Chunk.MAX_Y - Chunk.MIN_Y)) return null

        val overflowPositiveX = adjacentX >= Chunk.SIZE_X
        val overflowNegativeX = adjacentX < 0
        val overflowPositiveZ = adjacentZ >= Chunk.SIZE_Z
        val overflowNegativeZ = adjacentZ < 0

        val chunkX = chunk.x + if (overflowPositiveX) 1 else if (overflowNegativeX) -1 else 0
        val chunkZ = chunk.z + if (overflowPositiveZ) 1 else if (overflowNegativeZ) -1 else 0

        return if (chunkX == chunk.x && chunkZ == chunk.z) {
            chunk.getBlock(adjacentX, adjacentY, adjacentZ)
        } else {
            adjacentX = Math.floorMod(adjacentX, Chunk.SIZE_X)
            adjacentZ = Math.floorMod(adjacentZ, Chunk.SIZE_Z)
            chunk.world.getChunk(chunkX, chunkZ)?.getBlock(adjacentX, adjacentY, adjacentZ)
        }
    }
}

data class BlockAttributes(
    val name: String,
    val value: Any
)

sealed class BlockFace(val relativeX: Int, val relativeY: Int, val relativeZ: Int) {
    object TOP: BlockFace(0, 1, 0)
    object BOTTOM: BlockFace(0, -1, 0)
    object LEFT: BlockFace(-1, 0, 0)
    object RIGHT: BlockFace(1, 0, 0)
    object FRONT: BlockFace(0, 0, -1)
    object BACK: BlockFace(0, 0, 1)

    companion object{
        private val values = listOf(TOP, BOTTOM, LEFT, RIGHT, FRONT, BACK)
        fun values() = values
    }
}