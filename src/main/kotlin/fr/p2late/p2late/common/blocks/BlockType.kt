package fr.p2late.p2late.common.blocks

enum class BlockType(
    val id: String
) {
    AIR("p2l:air"),
    BEDROCK("p2l:bedrock"),
    DEEPSTONE("p2l:deepstone"),
    STONE("p2l:stone"),
    DIRT("p2l:dirt");

    override fun toString() = id
}