package fr.p2late.p2late.client.entity

import fr.p2late.p2late.client.graphic.MaterialModel
import org.joml.Matrix4f
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL13
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL30

class EntityRenderer(
    private val shader: EntityShader
) {
    fun prepare(){
        GL11.glEnable(GL11.GL_CULL_FACE)
        GL11.glCullFace(GL11.GL_BACK)
        GL11.glEnable(GL11.GL_DEPTH_TEST)
    }

    fun render(entities : Map<MaterialModel, List<Entity>>){
        for(model in entities.keys){
            prepareTexturedModel(model)
            val batch = entities[model] ?: continue
            for(entity in batch){
                prepareEntity(entity)
                GL11.glDrawElements(GL30.GL_TRIANGLES, model.rawModel.vertexCount, GL30.GL_UNSIGNED_INT, 0)
            }
            unbindTexturedModel()
        }
    }

    private fun prepareTexturedModel(texturedModel: MaterialModel){
        val rawModel = texturedModel.rawModel
        GL30.glBindVertexArray(rawModel.vaoID)
        GL20.glEnableVertexAttribArray(0)
        GL20.glEnableVertexAttribArray(1)
        GL20.glEnableVertexAttribArray(2)
        val material = texturedModel.material
        shader.setReflectivity(material.reflectivity)
        shader.setShineDamper(material.shineDamper)
        shader.setTextureUnit(0)
        GL13.glActiveTexture(0)
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, material.textureID)
    }
    private fun unbindTexturedModel(){
        GL20.glDisableVertexAttribArray(0)
        GL20.glDisableVertexAttribArray(1)
        GL20.glDisableVertexAttribArray(2)
        GL30.glBindVertexArray(0)
    }
    private fun prepareEntity(entity: Entity){
        val modelMatrix = Matrix4f()
            .scale(entity.scale)
            .rotateX(entity.rotation.x)
            .rotateY(entity.rotation.y)
            .rotateZ(entity.rotation.z)
            .translate(entity.position)
        shader.setModelMatrix(modelMatrix)
    }
}