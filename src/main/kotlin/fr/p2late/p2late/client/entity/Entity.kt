package fr.p2late.p2late.client.entity

import fr.p2late.p2late.client.graphic.MaterialModel
import org.joml.Vector3f

data class Entity(
    val model: MaterialModel,
    val position: Vector3f,
    val rotation: Vector3f,
    val scale: Float
)