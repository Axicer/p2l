package fr.p2late.p2late.client.entity

import fr.p2late.p2late.client.graphic.shader.Shader
import fr.p2late.p2late.client.graphic.shader.ShaderProgram
import fr.p2late.p2late.client.graphic.texture.Color
import org.joml.Matrix4f
import org.joml.Vector3f
import org.lwjgl.opengl.GL20

class EntityShader : ShaderProgram() {

    private val perspectiveMatrixLocation by lazy { getUniformLocation("perspectiveMatrix") }
    private val viewMatrixLocation by lazy { getUniformLocation("viewMatrix") }
    private val modelMatrixLocation by lazy { getUniformLocation("modelMatrix") }
    private val lightPositionLocation by lazy { getUniformLocation("lightPosition") }
    private val lightColorLocation by lazy { getUniformLocation("lightColor") }
    private val textureUnitLocation by lazy { getUniformLocation("textureSampler") }
    private val reflectivityLocation by lazy { getUniformLocation("reflectivity") }
    private val shineDamperLocation by lazy { getUniformLocation("shineDamper") }

    init {
        val vertexShader = Shader.loadShader(GL20.GL_VERTEX_SHADER, "/shaders/entity.vsh")
        val fragmentShader = Shader.loadShader(GL20.GL_FRAGMENT_SHADER, "/shaders/entity.fsh")
        attachShader(vertexShader)
        attachShader(fragmentShader)
        link()
        vertexShader.delete()
        fragmentShader.delete()
    }

    fun setPerspectiveMatrix(matrix4f: Matrix4f){
        setUniform(perspectiveMatrixLocation, matrix4f)
    }
    fun setViewMatrix(matrix4f: Matrix4f){
        setUniform(viewMatrixLocation, matrix4f)
    }
    fun setModelMatrix(matrix4f: Matrix4f){
        setUniform(modelMatrixLocation, matrix4f)
    }
    fun setLightPosition(vector3f: Vector3f){
        setUniform(lightPositionLocation, vector3f)
    }
    fun setLightColor(color: Color){
        setUniform(lightColorLocation, color)
    }
    fun setTextureUnit(unit: Int){
        setUniform(textureUnitLocation, unit)
    }
    fun setReflectivity(value: Float){
        setUniform(reflectivityLocation, value)
    }
    fun setShineDamper(value: Float){
        setUniform(shineDamperLocation, value)
    }


}