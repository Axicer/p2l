package fr.p2late.p2late.client.entity

import fr.p2late.p2late.client.Camera
import fr.p2late.p2late.client.graphic.MaterialModel
import fr.p2late.p2late.client.terrain.LightSource

object EntityMasterRenderer{
    private var shader = EntityShader()
    private val renderer = EntityRenderer(shader)

    private val entities = HashMap<MaterialModel, MutableList<Entity>>()

    fun processEntity(entity: Entity){
        val model = entity.model
        val batch = entities[model]
        if(batch != null){
            batch.add(entity)
        }else{
            entities[model] = mutableListOf(entity)
        }
    }

    fun render(lightSource: LightSource, camera: Camera){
        renderer.prepare()
        shader.start()
        shader.setLightColor(lightSource.color)
        shader.setLightPosition(lightSource.position)
        shader.setViewMatrix(camera.getViewMatrix())
        shader.setPerspectiveMatrix(camera.perspectiveMatrix)
        renderer.render(entities)
        shader.stop()
        entities.clear()
    }

    fun dispose(){
        shader.delete()
    }

}