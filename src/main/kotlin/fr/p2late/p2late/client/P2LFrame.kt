package fr.p2late.p2late.client

import fr.p2late.p2late.App
import fr.p2late.p2late.client.control.Controls
import fr.p2late.p2late.client.graphic.text.TextRenderer
import fr.p2late.p2late.client.gui.GUIRenderer
import fr.p2late.p2late.common.toGLFWBoolean
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GL11

typealias GLFWWindow = Long

class P2LFrame(
    private var width: Int,
    private var height: Int,
    title: CharSequence = App.appName,
    hidden: Boolean = false,
    resizable: Boolean = false,
    var vsync: Boolean = false
) {
    private var window: GLFWWindow = -1L

    init{
        /* Creating a temporary window for getting the available OpenGL version */
        GLFW.glfwDefaultWindowHints()
        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE)
        val temp = GLFW.glfwCreateWindow(1, 1, "", 0, 0)
        GLFW.glfwMakeContextCurrent(temp)
        GL.createCapabilities()
        val caps = GL.getCapabilities()
        GLFW.glfwDestroyWindow(temp)

        GLFW.glfwDefaultWindowHints()
        if (caps.OpenGL32) {
            /* Hints for OpenGL 3.2 core profile */
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, 3)
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, 2)
            GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_PROFILE, GLFW.GLFW_OPENGL_CORE_PROFILE)
            GLFW.glfwWindowHint(GLFW.GLFW_OPENGL_FORWARD_COMPAT, GLFW.GLFW_TRUE)
        } else if (caps.OpenGL21) {
            /* Hints for legacy OpenGL 2.1 */
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MAJOR, 2)
            GLFW.glfwWindowHint(GLFW.GLFW_CONTEXT_VERSION_MINOR, 1)
        } else {
            throw RuntimeException("Neither OpenGL 3.2 nor OpenGL 2.1 is supported, you may want to update your graphics driver.")
        }

        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, (!hidden).toGLFWBoolean())
        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, resizable.toGLFWBoolean())
        window = GLFW.glfwCreateWindow(width, height, title, 0, 0)
        if ( window == 0L ){
            GLFW.glfwTerminate()
            throw RuntimeException("Failed to create the GLFW window")
        }

        /* Center window on screen */
        val videoMode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor())
        GLFW.glfwSetWindowPos(
            window,
            (videoMode!!.width() - width) / 2,
            (videoMode.height() - height) / 2
        )

        /* Create OpenGL context */
        GLFW.glfwMakeContextCurrent(window)
        GL.createCapabilities()

        setVSync(vsync)

        GLFW.glfwSetKeyCallback(window, Controls)
        GLFW.glfwSetWindowSizeCallback(window) { _: Long, newWidth: Int, newHeight: Int ->
            val horizontalRatio = newWidth.toFloat() / width
            val verticalRatio = newHeight.toFloat() / height
            width = newWidth
            height = newHeight
            GL11.glViewport(0, 0, newWidth, newHeight)
            GUIRenderer.updateScreenRatio(horizontalRatio, verticalRatio)
            P2LClient.camera.computePerspectiveMatrix()
            TextRenderer.updateScreenSize(newWidth.toFloat(), newHeight.toFloat())
        }
//        GLUtil.setupDebugMessageCallback()
    }

    fun destroy() {
        GLFW.glfwDestroyWindow(window)
    }

    fun isClosing() = GLFW.glfwWindowShouldClose(window)

    private fun setVSync(vsync: Boolean) {
        this.vsync = vsync
        if (vsync) {
            GLFW.glfwSwapInterval(1)
        } else {
            GLFW.glfwSwapInterval(0)
        }
    }
}