package fr.p2late.p2late.client

import fr.p2late.p2late.client.control.Controls
import org.joml.Matrix4f
import org.joml.Vector3f
import org.lwjgl.glfw.GLFW

class Camera(
    var position: Vector3f,
    var rotation: Vector3f,
    val zNear: Float = 0.1f,
    val zFar: Float = 500f,
    val fov: Float = Math.toRadians(75.0).toFloat()
) {
    companion object {
        const val SPEED = 20f
        const val ROT_SPEED = 45f
    }

    lateinit var perspectiveMatrix: Matrix4f
    init {
        computePerspectiveMatrix()
    }
    fun computePerspectiveMatrix(){
        val (width, height) = P2LClient.getScreenSize()
        perspectiveMatrix = Matrix4f().perspective(fov, width.toFloat()/height, zNear, zFar)
    }


    fun update(dt: Float){
        var x = 0f
        var y = 0f
        var z = 0f
        if (GLFW.glfwGetKey(GLFW.glfwGetCurrentContext(), GLFW.GLFW_KEY_Z) == GLFW.GLFW_PRESS) {
            x += sin(rotation.y)
            z -= cos(rotation.y)
        }
        if (GLFW.glfwGetKey(GLFW.glfwGetCurrentContext(), GLFW.GLFW_KEY_Q) == GLFW.GLFW_PRESS) {
            x -= cos(rotation.y)
            z -= sin(rotation.y)
        }
        if (GLFW.glfwGetKey(GLFW.glfwGetCurrentContext(), GLFW.GLFW_KEY_S) == GLFW.GLFW_PRESS) {
            x -= sin(rotation.y)
            z += cos(rotation.y)
        }
        if (GLFW.glfwGetKey(GLFW.glfwGetCurrentContext(), GLFW.GLFW_KEY_D) == GLFW.GLFW_PRESS) {
            x += cos(rotation.y)
            z += sin(rotation.y)
        }
        if (GLFW.glfwGetKey(GLFW.glfwGetCurrentContext(), GLFW.GLFW_KEY_SPACE) == GLFW.GLFW_PRESS) {
            y = 1f
        }
        if (Controls.isShiftDown) {
            y = -1f
        }

        if(x != 0f || y != 0f || z != 0f){
            val vec = Vector3f(x,y,z).normalize().mul(dt * SPEED)
            move(vec.x, vec.y, vec.z)
        }

        x = 0f
        y = 0f
        z = 0f
        if (GLFW.glfwGetKey(GLFW.glfwGetCurrentContext(), GLFW.GLFW_KEY_UP) == GLFW.GLFW_PRESS && !Controls.isControlDown) {
            x -= 1f
        }
        if (GLFW.glfwGetKey(GLFW.glfwGetCurrentContext(), GLFW.GLFW_KEY_DOWN) == GLFW.GLFW_PRESS && !Controls.isControlDown) {
            x += 1f
        }
        if (GLFW.glfwGetKey(GLFW.glfwGetCurrentContext(), GLFW.GLFW_KEY_LEFT) == GLFW.GLFW_PRESS) {
            y -= 1f
        }
        if (GLFW.glfwGetKey(GLFW.glfwGetCurrentContext(), GLFW.GLFW_KEY_RIGHT) == GLFW.GLFW_PRESS) {
            y += 1f
        }
        if (GLFW.glfwGetKey(GLFW.glfwGetCurrentContext(), GLFW.GLFW_KEY_UP) == GLFW.GLFW_PRESS && Controls.isControlDown) {
            z += 1f
        }
        if (GLFW.glfwGetKey(GLFW.glfwGetCurrentContext(), GLFW.GLFW_KEY_DOWN) == GLFW.GLFW_PRESS && Controls.isControlDown) {
            z -= 1f
        }

        if(x != 0f || y != 0f || z != 0f){
            val vec = Vector3f(x,y,z).normalize().mul(dt * ROT_SPEED)
            rotate(vec.x, vec.y, vec.z)
        }
    }

    fun move(dx: Float = 0f, dy: Float = 0f, dz: Float = 0f) {
        position.x += dx
        position.y += dy
        position.z += dz
    }

    fun rotate(dpitch: Float = 0f, dyaw: Float = 0f, droll: Float = 0f) {
        rotation.x = (rotation.x + dpitch).mod(360f)
        rotation.y = (rotation.y + dyaw).mod(360f)
        rotation.z = (rotation.z + droll).mod(360f)
    }

    fun getViewMatrix(): Matrix4f{
        val viewMatrix = Matrix4f()
        viewMatrix.rotateX(rad(rotation.x))
        viewMatrix.rotateY(rad(rotation.y))
        viewMatrix.rotateZ(rad(rotation.z))
        viewMatrix.translate(-position.x, -position.y, -position.z)
        return viewMatrix
    }

    private fun cos(value: Float) = kotlin.math.cos(rad(value).toDouble()).toFloat()
    private fun sin(value: Float) = kotlin.math.sin(rad(value).toDouble()).toFloat()
    private fun rad(value: Float) = Math.toRadians(value.toDouble()).toFloat()

    override fun toString(): String {
        return "Camera pos=[${position.x},${position.y},${position.z}] rot=[${rotation.x},${rotation.y},${rotation.z}]"
    }
}