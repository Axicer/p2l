package fr.p2late.p2late.client.graphic.texture

import fr.p2late.p2late.common.blocks.BlockFace
import fr.p2late.p2late.common.blocks.BlockType
import fr.p2late.p2late.common.getResourceAsInputStream
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL13
import org.lwjgl.opengl.GL30
import org.lwjgl.system.MemoryUtil
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import kotlin.math.max

object TerrainTextureAtlas {
    var id = GL11.glGenTextures()
    private val textures: MutableMap<String, AtlasCoordinates> = mutableMapOf()
    private var width = 0
    private var height = 0
    private var atlas = BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)

    fun addNewTexture(path: String) {
        if (textures[path] != null) return
        getResourceAsInputStream(path).use { inputStream ->
            var image = ImageIO.read(inputStream)
            val newAtlas =
                BufferedImage(width + image.width, max(height, image.height), BufferedImage.TYPE_INT_ARGB)

            val g = newAtlas.createGraphics()
            g.drawImage(atlas, 0, 0, null)
            g.drawImage(image, width, 0, null)
            g.dispose()
            textures[path] = AtlasCoordinates(
                width.toFloat(),
                0f,
                width + image.width.toFloat(),
                max(height, image.height).toFloat()
            )

            width = newAtlas.width
            height = newAtlas.height
            atlas = newAtlas
        }
    }

    fun getAtlasCoordinates(texture: String) = textures[texture]?.let {
        AtlasCoordinates(
            it.s1 / width, it.t1 / height,
            it.s2 / width, it.t2 / height,
        )
    }

    fun createAtlas() {
        //ensure all texture are loaded before uploading texture
        BlockType.values().forEach {
            if (it != BlockType.AIR) {
                BlockTypeTexture.fromType(it)
                BlockTypeTexture.DEBUG
            }
        }
        ImageIO.write(atlas, "png", File("atlas.png"))

        val pixels = IntArray(width * height)
        atlas.getRGB(0, 0, width, height, pixels, 0, width)

        val buffer = MemoryUtil.memAlloc(width * height * 4)
        for (i in 0 until height) {
            for (j in 0 until width) {
                val pixel = pixels[i * width + j]
                buffer.put((pixel shr 16 and 0xFF).toByte())
                buffer.put((pixel shr 8 and 0xFF).toByte())
                buffer.put((pixel and 0xFF).toByte())
                buffer.put((pixel shr 24 and 0xFF).toByte())
            }
        }
        buffer.flip()

        GL11.glBindTexture(GL11.GL_TEXTURE_2D, id)
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL13.GL_TEXTURE_WRAP_S, GL13.GL_CLAMP_TO_BORDER)
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL13.GL_TEXTURE_WRAP_T, GL13.GL_CLAMP_TO_BORDER)
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL13.GL_TEXTURE_MIN_FILTER, GL13.GL_NEAREST_MIPMAP_LINEAR)
        GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL13.GL_TEXTURE_MAG_FILTER, GL13.GL_NEAREST)
        GL11.glTexImage2D(
            GL13.GL_TEXTURE_2D,
            0,
            GL13.GL_RGBA8,
            width,
            height,
            0,
            GL13.GL_RGBA,
            GL11.GL_UNSIGNED_BYTE,
            buffer
        )
        GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D)
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0)

        MemoryUtil.memFree(buffer)
    }

    fun dispose() {
        GL11.glDeleteTextures(id)
    }
}

data class AtlasCoordinates(
    val s1: Float,
    val t1: Float,
    val s2: Float,
    val t2: Float,
)

sealed class BlockTypeTexture(
    private val topTexture: String,
    private val bottomTexture: String,
    private val leftTexture: String,
    private val rightTexture: String,
    private val frontTexture: String,
    private val backTexture: String
) {
    init {
        TerrainTextureAtlas.addNewTexture(topTexture)
        TerrainTextureAtlas.addNewTexture(bottomTexture)
        TerrainTextureAtlas.addNewTexture(leftTexture)
        TerrainTextureAtlas.addNewTexture(rightTexture)
        TerrainTextureAtlas.addNewTexture(frontTexture)
        TerrainTextureAtlas.addNewTexture(backTexture)
    }

    open class SingleTextureBlockType(t: String) : BlockTypeTexture(t, t, t, t, t, t)
    open class TopBottomTextureBlockType(topBottom: String, t: String) :
        BlockTypeTexture(topBottom, topBottom, t, t, t, t)

    object DEBUG: TopBottomTextureBlockType("/textures/block/debug.png", "/textures/block/debug_face.png")
    object BEDROCK : SingleTextureBlockType("/textures/block/bedrock.png")
    object DIRT : SingleTextureBlockType("/textures/block/dirt.png")
    object STONE : SingleTextureBlockType("/textures/block/stone.png")
    object DEEPSTONE : TopBottomTextureBlockType("/textures/block/deepslate_top.png", "/textures/block/deepslate.png")

    fun getTextureCoords(face: BlockFace): AtlasCoordinates? {
        val texture = when (face) {
            BlockFace.TOP -> topTexture
            BlockFace.BOTTOM -> bottomTexture
            BlockFace.LEFT -> leftTexture
            BlockFace.RIGHT -> rightTexture
            BlockFace.FRONT -> frontTexture
            BlockFace.BACK -> backTexture
        }
        return TerrainTextureAtlas.getAtlasCoordinates(texture)
    }

    companion object {
        fun fromType(blockType: BlockType) = when (blockType) {
            BlockType.AIR -> throw IllegalStateException("Attempting to get AIR coordinates :(")
            BlockType.BEDROCK -> BEDROCK
            BlockType.DEEPSTONE -> DEEPSTONE
            BlockType.STONE -> STONE
            BlockType.DIRT -> DIRT
            else -> DEBUG
        }
    }
}