package fr.p2late.p2late.client.graphic.text

import fr.p2late.p2late.client.P2LClient
import fr.p2late.p2late.client.graphic.texture.Color
import fr.p2late.p2late.client.graphic.texture.Material
import fr.p2late.p2late.client.gui.component.AnchorPoint
import org.joml.Matrix4f
import org.lwjgl.opengl.*
import org.lwjgl.system.MemoryUtil
import java.nio.FloatBuffer

object TextRenderer {
    private val font = Font.loadFont("/fonts/Helvetica.ttf", 32)
    private val shader = TextShader()

    private val vaoID: Int
    private val vboID: Int
    private var vertices: FloatBuffer
    private var verticesCount = 0
    private var rendering = false

    init {
        shader.start()
        val (width, height) = P2LClient.getScreenSize()
        shader.setPerspectiveMatrix(Matrix4f().ortho2D(0f, width.toFloat(), 0f, height.toFloat()))
        vaoID = createVAO()
        vertices = MemoryUtil.memAllocFloat(4096)
        vboID = GL15.glGenBuffers()

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID)
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vertices.capacity().toLong() * Float.SIZE_BYTES, GL15.GL_DYNAMIC_DRAW)

        GL20.glEnableVertexAttribArray(0)
        GL20.glEnableVertexAttribArray(1)
        GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 4 * Float.SIZE_BYTES, 0)
        GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 4 * Float.SIZE_BYTES, 2 * Float.SIZE_BYTES.toLong())

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0)
        unbindVAO()
        shader.stop()
    }

    private fun createVAO() = GL30.glGenVertexArrays().also { GL30.glBindVertexArray(it) }
    private fun unbindVAO() {
        GL30.glBindVertexArray(0)
    }

    fun updateScreenSize(width: Float, height: Float){
        shader.start()
        shader.setPerspectiveMatrix(Matrix4f().ortho2D(0f, width, 0f, height))
        shader.stop()
    }

    private fun begin(){
        check(!rendering) { "Already rendering !" }
        rendering = true
        verticesCount = 0
        vertices.clear()
        shader.start()
    }

    private fun end(){
        check(rendering) { "Not rendering yet !" }
        rendering = false
        flushBuffer()
        shader.stop()
    }

    private fun flushBuffer(){
        if(verticesCount <= 0) return

        GL11.glDisable(GL11.GL_CULL_FACE)
        GL11.glEnable(GL11.GL_BLEND)
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA)

        vertices.flip()

//        print("[")
//        while(vertices.remaining() > 0){
//            print("${vertices.get()}, ")
//        }
//        println("]")

//        vertices.rewind()

        shader.start()
        GL30.glBindVertexArray(vaoID)
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID)
        GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, vertices)

        GL20.glEnableVertexAttribArray(0)
        GL20.glEnableVertexAttribArray(1)
        GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, verticesCount)
        GL20.glDisableVertexAttribArray(0)
        GL20.glDisableVertexAttribArray(1)

        shader.stop()
        vertices.clear()
        verticesCount = 0
    }

    fun getTextSize(text: CharSequence) = font.getTextSize(text)

    /**
     * Draw text at the specified position and color.
     *
     * @param text     Text to draw
     * @param x        X coordinate of the text position
     * @param y        Y coordinate of the text position
     * @param color    Color to use
     */
    fun drawText(text: CharSequence, x: Float, y: Float, color: Color = Color(), anchor: AnchorPoint) {
        val (screenWidth, screenHeight) = P2LClient.getScreenSize()
        val (textWidth, textHeight) = font.getTextSize(text)
        var drawX = x
        var drawY = y
        if (textHeight > font.fontHeight) {
            drawY += (textHeight - font.fontHeight).toFloat()
        }

        begin()
        shader.setColor(color)
        shader.setTextureUnit(0)
        GL13.glActiveTexture(GL13.GL_TEXTURE0)
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, font.texture.textureID)
        for (char in text) {
            if (char == '\n') {
                drawY -= font.fontHeight.toFloat()
                drawX = x
                continue
            }
            if (char == '\r') {
                continue
            }
            val (fX, fY) = anchor.convertCenterCoordinateToAnchorCoordinates(
                drawX + screenWidth/2 - textWidth/2,
                drawY + screenHeight/2 - textHeight/2
            )
            font.glyphs[char]?.let {
                drawGlyph(
                    font.texture,
                    fX, fY,
                    it.x.toFloat(), it.y.toFloat(),
                    it.width.toFloat(), it.height.toFloat()
                )
                drawX += it.width.toFloat()
            }
        }
        end()
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0)
    }

    private fun drawGlyph(
        material: Material,
        x: Float,
        y: Float,
        s: Float,
        t: Float,
        width: Float,
        height: Float
    ) {
        if(vertices.remaining() < 4 * 6){
            flushBuffer()
        }

        val x2 = x + width
        val y2 = y + height
        val s1: Float = s / material.width
        val t1: Float = t / material.height
        val s2: Float = (s + width) / material.width
        val t2: Float = (t + height) / material.height

        vertices.put(x).put(y).put(s1).put(t1)
        vertices.put(x).put(y2).put(s1).put(t2)
        vertices.put(x2).put(y2).put(s2).put(t2)

        vertices.put(x).put(y).put(s1).put(t1)
        vertices.put(x2).put(y2).put(s2).put(t2)
        vertices.put(x2).put(y).put(s2).put(t1)

        verticesCount += 6
    }

    fun dispose() {
        MemoryUtil.memFree(vertices)
        GL30.glDeleteVertexArrays(vaoID)
        GL15.glDeleteBuffers(vboID)
        shader.delete()
        font.dispose()
    }
}