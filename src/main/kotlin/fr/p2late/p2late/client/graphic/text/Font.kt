package fr.p2late.p2late.client.graphic.text

import fr.p2late.p2late.client.graphic.texture.Material
import fr.p2late.p2late.common.getResourceAsInputStream
import org.lwjgl.opengl.GL11
import org.lwjgl.system.MemoryUtil
import java.awt.Font
import java.awt.RenderingHints
import java.awt.geom.AffineTransform
import java.awt.image.AffineTransformOp
import java.awt.image.BufferedImage
import java.io.InputStream

data class Glyph(
    val width: Int,
    val height: Int,
    val x: Int,
    val y: Int,
)

class Font(
    font: Font = Font(Font.MONOSPACED, Font.PLAIN, 16),
    antiAlias: Boolean = true
) {
    val glyphs = HashMap<Char, Glyph>()
    val texture = createFontTexture(font, antiAlias)
    var fontHeight = 0

    constructor(
        stream: InputStream,
        size: Int,
        antiAlias: Boolean
    ) : this(Font.createFont(Font.TRUETYPE_FONT, stream).deriveFont(Font.PLAIN, size.toFloat()), antiAlias)

    private fun createFontTexture(font: Font, antiAlias: Boolean): Material {
        /* Loop through the characters to get charWidth and charHeight */
        var imageWidth = 0
        var imageHeight = 0

        //Start at char #32, because ASCII 0 to 31 are just control codes
        for (i in 32 until 256) {
            if (i == 127) continue //ASCII 127 is the DEL control code, so we can skip it

            val charImage = createCharImage(font, i.toChar(), antiAlias) ?: continue //skip this char if char image is null
            imageWidth += charImage.width
            imageHeight = imageHeight.coerceAtLeast(charImage.height)
        }
        fontHeight = imageHeight

        var image = BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB)
        val g = image.createGraphics()
        var x = 0

        for (charIndex in 32 until 256) {
            if (charIndex == 127) continue

            val char = charIndex.toChar()
            val charImage = createCharImage(font, char, antiAlias) ?: continue
            val charWidth = charImage.width
            val charHeight = charImage.height

            /* Create glyph and draw char on image */
            val glyph = Glyph(charWidth, charHeight, x, image.height - charHeight)
            g.drawImage(charImage, x, 0, null)
            x += glyph.width
            glyphs[char] = glyph
        }

        /* Flip image Horizontal to get the origin to bottom left */
        val transform = AffineTransform.getScaleInstance(1.0, -1.0)
        transform.translate(0.0, -image.height.toDouble())
        val operation = AffineTransformOp(transform, AffineTransformOp.TYPE_NEAREST_NEIGHBOR)
        image = operation.filter(image, null)

        /* Get charWidth and charHeight of image */
        val width = image.width
        val height = image.height

        /* Get pixel data of image */
        val pixels = IntArray(width * height)
        image.getRGB(0, 0, width, height, pixels, 0, width)

        /* Put pixel data into a ByteBuffer */
        val buffer = MemoryUtil.memAlloc(width * height * 4)
        for (i in 0 until height) {
            for (j in 0 until width) {
                val pixel = pixels[i * width + j]
                buffer.put((pixel shr 16 and 0xFF).toByte())
                buffer.put((pixel shr 8 and 0xFF).toByte())
                buffer.put((pixel and 0xFF).toByte())
                buffer.put((pixel shr 24 and 0xFF).toByte())
            }
        }
        buffer.flip()

        val fontTexture = Material.createMaterial(width, height, buffer, 0f, 0f)
        MemoryUtil.memFree(buffer)

//        ImageIO.write(image, "png", File("font.png"))

        return fontTexture
    }

    private fun createCharImage(font: Font, c: Char, antiAlias: Boolean): BufferedImage? {
        /* Creating temporary image to extract character size */
        var image = BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)
        var g = image.createGraphics()
        if (antiAlias) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        }
        g.font = font
        val metrics = g.fontMetrics
        g.dispose()

        val charWidth = metrics.charWidth(c)
        val charHeight = metrics.height
        if (charWidth == 0) return null

        image = BufferedImage(charWidth, charHeight, BufferedImage.TYPE_INT_ARGB)
        g = image.createGraphics()
        if (antiAlias) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        }
        g.font = font
        g.paint = java.awt.Color.WHITE
        g.drawString(c.toString(), 0, metrics.ascent)
        g.dispose()
        return image
    }

    fun getTextSize(text: CharSequence): Pair<Int, Int> {
        var width = 0
        var lineWidth = 0
        var height = 0
        var lineHeight = 0
        for (char in text) {
            if (char == '\n') {
                /* Line end, add line height to stored height */
                height += lineHeight
                lineHeight = 0
                width = width.coerceAtLeast(lineWidth)
                lineWidth = 0
                continue
            }
            if (char == '\r') {
                /* Carriage return, just skip it */
                continue
            }
            glyphs[char]?.let {
                lineHeight = lineHeight.coerceAtLeast(it.height)
                lineWidth += it.width
            }
        }
        height += lineHeight
        width = width.coerceAtLeast(lineWidth)
        return width to height
    }

    fun dispose() {
        GL11.glDeleteTextures(texture.textureID)
    }

    companion object{
        fun loadFont(src: String, size: Int) = Font(getResourceAsInputStream(src), size, true)
    }
}