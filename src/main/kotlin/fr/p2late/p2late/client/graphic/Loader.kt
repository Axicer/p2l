package fr.p2late.p2late.client.graphic

import fr.p2late.p2late.client.graphic.texture.CubeMaterial
import fr.p2late.p2late.common.getResourceAsText
import org.joml.Vector2f
import org.joml.Vector3f
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL15
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL30

object Loader {

    private fun loadToRawModel(
        positions: FloatArray,
        textureCoords: FloatArray,
        normals: FloatArray?,
        indices: IntArray
    ): RawModel {
        val vao = createVAO()
        storeElementIndices(indices)
        storeDataInAttributeList(0, 3, positions)
        storeDataInAttributeList(1, 2, textureCoords)
        if (normals != null) {
            storeDataInAttributeList(2, 3, normals)
        }
        unbindVAO()
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0)
        return RawModel(vao, indices.size)
    }

    fun loadGUI(positions: FloatArray): GUIModel {
        val vao = createVAO()
        storeDataInAttributeList(0, 2, positions)
        unbindVAO()
        return GUIModel(vao, positions.size / 2)
    }

    fun loadSkybox(vararg textures: String): CubeMaterialModel {
        val vao = createVAO()
        storeDataInAttributeList(0, 3, floatArrayOf(0f, 0f, 0f))
        unbindVAO()
        return CubeMaterialModel(RawModel(vao, 1), CubeMaterial.loadCubeMaterial(*textures))
    }

    private fun createVAO() = GL30.glGenVertexArrays().also { GL30.glBindVertexArray(it) }
    private fun unbindVAO() {
        GL30.glBindVertexArray(0)
    }

    private fun storeElementIndices(indices: IntArray) {
        val vbo = GL15.glGenBuffers()
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vbo)
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indices, GL15.GL_STATIC_DRAW)
    }

    private fun storeDataInAttributeList(attributeNumber: Int, size: Int, data: FloatArray) {
        val vbo = GL15.glGenBuffers()
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo)
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, data, GL15.GL_STATIC_DRAW)
        GL20.glVertexAttribPointer(attributeNumber, size, GL11.GL_FLOAT, false, 0, 0)
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0)
    }

    fun loadOBJFile(objFilePath: String): RawModel {
        val lines = getResourceAsText(objFilePath)

        val vertices = mutableListOf<Vector3f>()
        val textureCoords = mutableListOf<Vector2f>()
        val normals = mutableListOf<Vector3f>()
        val indices = mutableListOf<Int>()

        var textureCoordsArray = FloatArray(0)
        var normalsArray = FloatArray(0)
        var firstFace = true
        for (line in lines) {
            if (line.startsWith("#")) continue
            val lineValues = line.split("\\s".toRegex())
            when {
                line.startsWith("v ") -> {
                    vertices.add(Vector3f(lineValues[1].toFloat(), lineValues[2].toFloat(), lineValues[3].toFloat()))
                }

                line.startsWith("vt ") -> {
                    textureCoords.add(Vector2f(lineValues[1].toFloat(), lineValues[2].toFloat()))
                }

                line.startsWith("vn") -> {
                    normals.add(Vector3f(lineValues[1].toFloat(), lineValues[2].toFloat(), lineValues[3].toFloat()))
                }

                line.startsWith("f ") -> {
                    if (firstFace) {
                        println("processed ${vertices.size} vertices, ${textureCoords.size} texture coordinates, ${normals.size} normals value")
                        textureCoordsArray = FloatArray(vertices.size * 2)
                        normalsArray = FloatArray(vertices.size * 3)
                        firstFace = false
                    }

                    val vertex1 = lineValues[1].split("/")
                    val vertex2 = lineValues[2].split("/")
                    val vertex3 = lineValues[3].split("/")

                    processVertex(vertex1, indices, textureCoords, normals, textureCoordsArray, normalsArray)
                    processVertex(vertex2, indices, textureCoords, normals, textureCoordsArray, normalsArray)
                    processVertex(vertex3, indices, textureCoords, normals, textureCoordsArray, normalsArray)

                    if (lineValues.size == 5) {
                        //then obj is describing quads instead of triangles, so we split quads into 2 triangles
                        val vertex4 = lineValues[4].split("/")
                        processVertex(vertex1, indices, textureCoords, normals, textureCoordsArray, normalsArray)
                        processVertex(vertex3, indices, textureCoords, normals, textureCoordsArray, normalsArray)
                        processVertex(vertex4, indices, textureCoords, normals, textureCoordsArray, normalsArray)
                    }
                }

                else -> continue
            }
        }

        val verticesArray = FloatArray(vertices.size * 3)
        val indicesArray = indices.toIntArray()

        var vertexPointer = 0
        for (vertex in vertices) {
            verticesArray[vertexPointer++] = vertex.x
            verticesArray[vertexPointer++] = vertex.y
            verticesArray[vertexPointer++] = vertex.z
        }

        return loadToRawModel(verticesArray, textureCoordsArray, normalsArray, indicesArray)
    }

    private fun processVertex(
        vertexData: List<String>,
        indices: MutableList<Int>,
        textureCoords: List<Vector2f>,
        normals: List<Vector3f>,
        textureCoordsArray: FloatArray,
        normalsArray: FloatArray
    ) {
        val currentVertexPointer = vertexData[0].toInt() - 1 //OBJ start at 1
        indices.add(currentVertexPointer)
        val texCoords = textureCoords[vertexData[1].toInt() - 1] //OBJ start at 1
        textureCoordsArray[currentVertexPointer * 2] = texCoords.x
        textureCoordsArray[currentVertexPointer * 2 + 1] = texCoords.y
        val normal = normals[vertexData[2].toInt() - 1] //OBJ start at 1
        normalsArray[currentVertexPointer * 3] = normal.x
        normalsArray[currentVertexPointer * 3 + 1] = normal.y
        normalsArray[currentVertexPointer * 3 + 2] = normal.z
    }

}