package fr.p2late.p2late.client.graphic.texture

import fr.p2late.p2late.common.getResourceAsByteBuffer
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL13
import org.lwjgl.stb.STBImage
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer

class Material(
    var width: Int,
    var height: Int,
    var reflectivity: Float,
    var shineDamper: Float
){
    var textureID = GL13.glGenTextures()

    companion object {
        fun createMaterial(width: Int, height: Int, buffer: ByteBuffer, reflectivity: Float, shineDamper: Float) : Material{
            val material = Material(width, height, reflectivity, shineDamper)
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, material.textureID)
            GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL13.GL_TEXTURE_WRAP_S, GL13.GL_CLAMP_TO_BORDER)
            GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL13.GL_TEXTURE_WRAP_T, GL13.GL_CLAMP_TO_BORDER)
            GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL13.GL_TEXTURE_MIN_FILTER, GL13.GL_NEAREST)
            GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL13.GL_TEXTURE_MAG_FILTER, GL13.GL_NEAREST)
            GL11.glTexImage2D(GL13.GL_TEXTURE_2D, 0, GL13.GL_RGBA8, width, height, 0, GL13.GL_RGBA, GL11.GL_UNSIGNED_BYTE, buffer)
            GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0)

            return material
        }

        fun loadMaterial(path: String, reflectivity: Float, shineDamper: Float): Material {
            var image: ByteBuffer?
            var width: Int
            var height: Int
            var material: Material
            MemoryStack.stackPush().use { stack ->
                /* Prepare image buffers */
                val w = stack.mallocInt(1)
                val h = stack.mallocInt(1)
                val comp = stack.mallocInt(1)

                /* Load image */
                STBImage.stbi_set_flip_vertically_on_load(true)
                image = STBImage.stbi_load_from_memory(getResourceAsByteBuffer(path), w, h, comp, 4)
                if (image == null) {
                    throw RuntimeException("Failed to load a texture file!${System.lineSeparator()}${STBImage.stbi_failure_reason()}")
                }

                /* Get width and height of image */
                width = w.get()
                height = h.get()
                material = createMaterial(width, height, image!!, reflectivity, shineDamper)
                STBImage.stbi_image_free(image!!)
            }
            return material
        }
    }
}