package fr.p2late.p2late.client.graphic.texture

import fr.p2late.p2late.common.getResourceAsByteBuffer
import org.lwjgl.opengl.GL13.*
import org.lwjgl.opengl.GL30
import org.lwjgl.stb.STBImage
import org.lwjgl.system.MemoryStack
import java.nio.ByteBuffer

class CubeMaterial {
    val id = glGenTextures()

    private fun bind() {
        glBindTexture(GL_TEXTURE_CUBE_MAP, id)
    }

    private fun unbind(){
        glBindTexture(GL_TEXTURE_CUBE_MAP, 0)
    }

    private fun setParameter(name: Int, value: Int) {
        glTexParameteri(GL_TEXTURE_CUBE_MAP, name, value)
    }

    private fun uploadData(cubeIndex: Int, internalFormat: Int, width: Int, height: Int, format: Int, data: ByteBuffer) {
        glTexImage2D(
            GL_TEXTURE_CUBE_MAP_POSITIVE_X + cubeIndex,
            0,
            internalFormat,
            width,
            height,
            0,
            format,
            GL_UNSIGNED_BYTE,
            data
        )
    }

    companion object {
        private const val textureUnit = GL_TEXTURE0

        fun loadCubeMaterial(vararg path: String): CubeMaterial {
            val texture = CubeMaterial()

            glActiveTexture(textureUnit)
            texture.bind()

            for (i in path.indices) {
                var image: ByteBuffer?
                MemoryStack.stackPush().use { stack ->
                    /* Prepare image buffers */
                    val w = stack.mallocInt(1)
                    val h = stack.mallocInt(1)
                    val comp = stack.mallocInt(1)

                    /* Load image */
                    STBImage.stbi_set_flip_vertically_on_load(true)
                    image = STBImage.stbi_load_from_memory(getResourceAsByteBuffer(path[i]), w, h, comp, 4)
                    if (image == null) {
                        throw RuntimeException("Failed to load a texture file!${System.lineSeparator()}${STBImage.stbi_failure_reason()}")
                    }
                    texture.uploadData(i, GL_RGBA, w.get(), h.get(), GL_RGBA, image!!)
                    STBImage.stbi_image_free(image!!)
                }
            }

            texture.setParameter(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
            texture.setParameter(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
            texture.setParameter(GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE)
            texture.setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR)
            texture.setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR)
            GL30.glGenerateMipmap(GL_TEXTURE_CUBE_MAP)

            texture.unbind()

            return texture
        }
    }
}