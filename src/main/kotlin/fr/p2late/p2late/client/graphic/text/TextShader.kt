package fr.p2late.p2late.client.graphic.text

import fr.p2late.p2late.client.graphic.shader.Shader
import fr.p2late.p2late.client.graphic.shader.ShaderProgram
import fr.p2late.p2late.client.graphic.texture.Color
import org.joml.Matrix4f
import org.lwjgl.opengl.GL20

class TextShader : ShaderProgram() {

    private val perspectiveMatrixLocation by lazy { getUniformLocation("perspectiveMatrix") }
    private val textureUnitLocation by lazy { getUniformLocation("textureSampler") }
    private val colorLocation by lazy { getUniformLocation("color") }

    init {
        val vertexShader = Shader.loadShader(GL20.GL_VERTEX_SHADER, "/shaders/text.vsh")
        val fragmentShader = Shader.loadShader(GL20.GL_FRAGMENT_SHADER, "/shaders/text.fsh")
        attachShader(vertexShader)
        attachShader(fragmentShader)
        link()
        vertexShader.delete()
        fragmentShader.delete()
    }

    fun setPerspectiveMatrix(matrix4f: Matrix4f) {
        setUniform(perspectiveMatrixLocation, matrix4f)
    }

    fun setTextureUnit(unit: Int) {
        setUniform(textureUnitLocation, unit)
    }

    fun setColor(color: Color){
        setUniform(colorLocation, color)
    }

}