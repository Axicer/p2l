package fr.p2late.p2late.client.graphic

import fr.p2late.p2late.client.graphic.texture.CubeMaterial
import fr.p2late.p2late.client.graphic.texture.Material

open class RawModel(
    open val vaoID: Int,
    open val vertexCount: Int
)

class GUIModel(
    override val vaoID: Int,
    override val vertexCount: Int
) : RawModel(vaoID, vertexCount)

data class CubeMaterialModel(
    val rawModel: RawModel,
    val material: CubeMaterial
)

data class MaterialModel(
    val rawModel: RawModel,
    val material: Material
)