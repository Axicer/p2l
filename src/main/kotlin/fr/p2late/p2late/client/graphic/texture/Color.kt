package fr.p2late.p2late.client.graphic.texture

import java.util.*

data class Color(
    var r: Int = 255,
    var g: Int = 255,
    var b: Int = 255,
    var a: Int = 255
) {
    companion object {
        fun random(randomAlpha: Boolean = false): Color {
            val random = Random()
            return Color(
                random.nextInt(256),
                random.nextInt(256),
                random.nextInt(256),
                if (randomAlpha) random.nextInt(256) else 255
            )
        }
    }
}