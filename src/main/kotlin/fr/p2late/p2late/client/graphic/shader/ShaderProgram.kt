package fr.p2late.p2late.client.graphic.shader

import fr.p2late.p2late.client.graphic.texture.Color
import org.joml.Matrix4f
import org.joml.Vector2f
import org.joml.Vector3f
import org.joml.Vector4f
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL20

open class ShaderProgram {
    private val id: Int = GL20.glCreateProgram()

    fun attachShader(shader: Shader) {
        GL20.glAttachShader(id, shader.id)
    }

    fun link() {
        GL20.glLinkProgram(id)
        checkStatus()
    }

    fun getUniformLocation(name: CharSequence): Int {
        return GL20.glGetUniformLocation(id, name)
    }

    fun setUniform(location: Int, value: Int) {
        GL20.glUniform1i(location, value)
    }

    fun setUniform(location: Int, value: Float) {
        GL20.glUniform1f(location, value)
    }

    fun setUniform(location: Int, value: Vector2f) {
        GL20.glUniform2f(location, value.x, value.y)
    }

    fun setUniform(location: Int, value: Vector3f) {
        GL20.glUniform3f(location, value.x, value.y, value.z)
    }

    fun setUniform(location: Int, value: Vector4f) {
        GL20.glUniform4f(location, value.x, value.y, value.z, value.w)
    }

    fun setUniform(location: Int, value: Color) {
        GL20.glUniform4f(
            location,
            value.r.toFloat() / 255f,
            value.g.toFloat() / 255f,
            value.b.toFloat() / 255f,
            value.a.toFloat() / 255f
        )
    }

    fun setUniform(location: Int, value: Matrix4f) {
        val buffer = BufferUtils.createFloatBuffer(16)
        value.get(buffer)
        GL20.glUniformMatrix4fv(location, false, buffer)
    }

    fun start() {
        GL20.glUseProgram(id)
    }

    fun stop() {
        GL20.glUseProgram(0)
    }

    private fun checkStatus() {
        val status: Int = GL20.glGetProgrami(id, GL20.GL_LINK_STATUS)
        if (status != GL20.GL_TRUE) {
            throw RuntimeException(GL20.glGetProgramInfoLog(id))
        }
    }

    fun delete() {
        GL20.glDeleteProgram(id)
    }
}