package fr.p2late.p2late.client.terrain

import fr.p2late.p2late.client.Camera
import fr.p2late.p2late.client.graphic.texture.AtlasCoordinates
import fr.p2late.p2late.client.graphic.texture.BlockTypeTexture
import fr.p2late.p2late.client.graphic.texture.TerrainTextureAtlas
import fr.p2late.p2late.common.blocks.Block
import fr.p2late.p2late.common.blocks.BlockFace
import fr.p2late.p2late.common.blocks.BlockType
import fr.p2late.p2late.common.world.Chunk
import fr.p2late.p2late.common.world.World
import org.lwjgl.opengl.*
import org.lwjgl.system.MemoryUtil
import java.nio.FloatBuffer

class WorldRenderer(
    val world: World,
    val camera: Camera
) {

    companion object {
        //size is exactly one full chunk (a block is 6 faces of 2 triangles of 3 vertex
        const val MAX_BUFFER_SIZE =
            Chunk.SIZE_X * (Chunk.MAX_Y - Chunk.MIN_Y) * Chunk.SIZE_Z * 6 * 2 * 3
    }

    private val chunksRenderData: MutableMap<Chunk, RenderData> = mutableMapOf()
    private var currentRenderData: RenderData? = null

    private val vertices = MemoryUtil.memAllocFloat(MAX_BUFFER_SIZE * 3)
    private val normals = MemoryUtil.memAllocFloat(MAX_BUFFER_SIZE * 3)
    private val textureCoords = MemoryUtil.memAllocFloat(MAX_BUFFER_SIZE * 2)
    private val vaoId: Int
    private val vboId: Int
    private val nboId: Int
    private val tboId: Int
    private val shader = ChunkShader()

    init {
        shader.start()
        vaoId = GL30.glGenVertexArrays()
        GL30.glBindVertexArray(vaoId)

        vboId = GL15.glGenBuffers()
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId)
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, MAX_BUFFER_SIZE * 3 * Float.SIZE_BYTES.toLong(), GL15.GL_DYNAMIC_DRAW)
        GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0)

        nboId = GL15.glGenBuffers()
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, nboId)
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, MAX_BUFFER_SIZE * 3 * Float.SIZE_BYTES.toLong(), GL15.GL_DYNAMIC_DRAW)
        GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, 0, 0)

        tboId = GL15.glGenBuffers()
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, tboId)
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, MAX_BUFFER_SIZE * 2 * Float.SIZE_BYTES.toLong(), GL15.GL_DYNAMIC_DRAW)
        GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 0, 0)

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0)
        GL30.glBindVertexArray(0)
        shader.stop()
    }

    private fun flush(lightSource: LightSource) {
        if (currentRenderData == null) return

        vertices.clear()
        normals.clear()
        textureCoords.clear()
        currentRenderData!!.vertices.toBuffer(vertices)
        currentRenderData!!.normals.toBuffer(normals)
        currentRenderData!!.textureCoords.toBuffer(textureCoords)

        GL11.glEnable(GL11.GL_CULL_FACE)
        GL13.glActiveTexture(GL13.GL_TEXTURE0)
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, TerrainTextureAtlas.id)

        shader.start()
        shader.setAtlas(0)
        shader.setLight(lightSource)
        shader.setPerspectiveMatrix(camera.perspectiveMatrix)
        shader.setViewMatrix(camera.getViewMatrix())

        GL30.glBindVertexArray(vaoId)

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboId)
        GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, vertices)
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, nboId)
        GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, normals)
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, tboId)
        GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, textureCoords)

        GL20.glEnableVertexAttribArray(0)
        GL20.glEnableVertexAttribArray(1)
        GL20.glEnableVertexAttribArray(2)
        GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, currentRenderData!!.verticesCount)
        GL20.glDisableVertexAttribArray(0)
        GL20.glDisableVertexAttribArray(1)
        GL20.glDisableVertexAttribArray(2)

        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0)
        GL30.glBindVertexArray(0)
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0)
        shader.stop()
    }

    fun render(lightSource: LightSource) {
        for (chunk in world.loadedChunk.values) {
            renderChunk(chunk, lightSource)
        }
    }

    private fun renderChunk(chunk: Chunk, lightSource: LightSource) {
        currentRenderData = chunksRenderData[chunk] ?: run {
            val rd = computeRenderData(chunk)
            chunksRenderData[chunk] = rd
            return@run rd
        }

        flush(lightSource)
    }

    private fun computeRenderData(chunk: Chunk): RenderData {
        val vertexBuffer = MemoryUtil.memAllocFloat(MAX_BUFFER_SIZE * 3)
        val normalBuffer = MemoryUtil.memAllocFloat(MAX_BUFFER_SIZE * 3)
        val textureBuffer = MemoryUtil.memAllocFloat(MAX_BUFFER_SIZE * 2)
        var verticesCount = 0
        for (x in 0 until Chunk.SIZE_X) {
            for (z in 0 until Chunk.SIZE_Z) {
                for (y in 0 until (Chunk.MAX_Y - Chunk.MIN_Y)) {
                    val block = chunk.getBlock(x, y, z)
                    if (block != null && block.type != BlockType.AIR) {
                        for (face in BlockFace.values()) {
                            val adjacentBlock = block.getAdjacentBlock(face)
                            if (adjacentBlock == null || adjacentBlock.type == BlockType.AIR) {
                                renderFace(block, face, vertexBuffer, normalBuffer, textureBuffer)
                                verticesCount += 6
                            }
                        }
                    }
                }
            }
        }

        val renderData = RenderData(vertexBuffer.toArray(), normalBuffer.toArray(), textureBuffer.toArray(), verticesCount)

        MemoryUtil.memFree(vertexBuffer)
        MemoryUtil.memFree(normalBuffer)
        MemoryUtil.memFree(textureBuffer)

        return renderData
    }

    private fun renderFace(block: Block, blockFace: BlockFace, vertexBuffer: FloatBuffer, normalBuffer: FloatBuffer, textureBuffer: FloatBuffer) {
        val x = block.getWorldX().toFloat()
        val y = block.getWorldY().toFloat()
        val z = block.getWorldZ().toFloat()

        val d = 0.5f
        val tc = BlockTypeTexture.fromType(block.type).getTextureCoords(blockFace) ?: AtlasCoordinates(0f,0f,0f,0f)

        when (blockFace) {
            BlockFace.TOP -> {
                vertexBuffer.put(x - d).put(y + d).put(z + d)
                vertexBuffer.put(x + d).put(y + d).put(z - d)
                vertexBuffer.put(x - d).put(y + d).put(z - d)
                vertexBuffer.put(x + d).put(y + d).put(z + d)
                vertexBuffer.put(x + d).put(y + d).put(z - d)
                vertexBuffer.put(x - d).put(y + d).put(z + d)

                for (i in 0 until 6) {
                    normalBuffer.put(0f).put(1f).put(0f)
                }

                textureBuffer.put(tc.s1).put(tc.t2)
                textureBuffer.put(tc.s2).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t1)
                textureBuffer.put(tc.s2).put(tc.t2)
                textureBuffer.put(tc.s2).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t2)
            }

            BlockFace.BOTTOM -> {
                vertexBuffer.put(x + d).put(y - d).put(z - d)
                vertexBuffer.put(x + d).put(y - d).put(z + d)
                vertexBuffer.put(x - d).put(y - d).put(z + d)
                vertexBuffer.put(x + d).put(y - d).put(z - d)
                vertexBuffer.put(x - d).put(y - d).put(z + d)
                vertexBuffer.put(x - d).put(y - d).put(z - d)

                for (i in 0 until 6) {
                    normalBuffer.put(0f).put(-1f).put(0f)
                }

                textureBuffer.put(tc.s2).put(tc.t1)
                textureBuffer.put(tc.s2).put(tc.t2)
                textureBuffer.put(tc.s1).put(tc.t2)
                textureBuffer.put(tc.s2).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t2)
                textureBuffer.put(tc.s1).put(tc.t1)
            }

            BlockFace.LEFT -> {
                vertexBuffer.put(x - d).put(y - d).put(z + d)
                vertexBuffer.put(x - d).put(y + d).put(z + d)
                vertexBuffer.put(x - d).put(y + d).put(z - d)
                vertexBuffer.put(x - d).put(y - d).put(z + d)
                vertexBuffer.put(x - d).put(y + d).put(z - d)
                vertexBuffer.put(x - d).put(y - d).put(z - d)

                for (i in 0 until 6) {
                    normalBuffer.put(-1f).put(0f).put(0f)
                }

                textureBuffer.put(tc.s2).put(tc.t2)
                textureBuffer.put(tc.s2).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t1)
                textureBuffer.put(tc.s2).put(tc.t2)
                textureBuffer.put(tc.s1).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t2)
            }

            BlockFace.RIGHT -> {
                vertexBuffer.put(x + d).put(y - d).put(z - d)
                vertexBuffer.put(x + d).put(y + d).put(z - d)
                vertexBuffer.put(x + d).put(y - d).put(z + d)
                vertexBuffer.put(x + d).put(y + d).put(z - d)
                vertexBuffer.put(x + d).put(y + d).put(z + d)
                vertexBuffer.put(x + d).put(y - d).put(z + d)

                for (i in 0 until 6) {
                    normalBuffer.put(1f).put(0f).put(0f)
                }

                textureBuffer.put(tc.s2).put(tc.t2)
                textureBuffer.put(tc.s2).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t2)
                textureBuffer.put(tc.s2).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t2)
            }

            BlockFace.FRONT -> {
                vertexBuffer.put(x - d).put(y + d).put(z - d)
                vertexBuffer.put(x + d).put(y + d).put(z - d)
                vertexBuffer.put(x + d).put(y - d).put(z - d)
                vertexBuffer.put(x - d).put(y + d).put(z - d)
                vertexBuffer.put(x + d).put(y - d).put(z - d)
                vertexBuffer.put(x - d).put(y - d).put(z - d)

                for (i in 0 until 6) {
                    normalBuffer.put(0f).put(0f).put(1f)
                }

                textureBuffer.put(tc.s2).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t2)
                textureBuffer.put(tc.s2).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t2)
                textureBuffer.put(tc.s2).put(tc.t2)
            }

            BlockFace.BACK -> {
                vertexBuffer.put(x - d).put(y + d).put(z + d)
                vertexBuffer.put(x - d).put(y - d).put(z + d)
                vertexBuffer.put(x + d).put(y - d).put(z + d)
                vertexBuffer.put(x + d).put(y - d).put(z + d)
                vertexBuffer.put(x + d).put(y + d).put(z + d)
                vertexBuffer.put(x - d).put(y + d).put(z + d)

                for (i in 0 until 6) {
                    normalBuffer.put(0f).put(0f).put(-1f)
                }

                textureBuffer.put(tc.s1).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t2)
                textureBuffer.put(tc.s2).put(tc.t2)
                textureBuffer.put(tc.s2).put(tc.t2)
                textureBuffer.put(tc.s2).put(tc.t1)
                textureBuffer.put(tc.s1).put(tc.t1)
            }
        }
    }

    fun dispose() {
        MemoryUtil.memFree(vertices)
        MemoryUtil.memFree(normals)
        MemoryUtil.memFree(textureCoords)
    }
}

private fun FloatArray.toBuffer(buffer: FloatBuffer){
    buffer.put(this)
    buffer.flip()
}

private fun FloatBuffer.toArray(): FloatArray {
    flip()
    val array = FloatArray(remaining())
    for (i in array.indices) {
        array[i] = get()
    }
    rewind()
    return array
}

private data class RenderData(
    var vertices: FloatArray,
    var normals: FloatArray,
    var textureCoords: FloatArray,
    var verticesCount: Int = 0
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RenderData

        if (!vertices.contentEquals(other.vertices)) return false
        if (!normals.contentEquals(other.normals)) return false
        if (verticesCount != other.verticesCount) return false

        return true
    }

    override fun hashCode(): Int {
        var result = vertices.contentHashCode()
        result = 31 * result + normals.contentHashCode()
        result = 31 * result + verticesCount
        return result
    }
}