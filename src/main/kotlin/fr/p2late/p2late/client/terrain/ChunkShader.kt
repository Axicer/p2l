package fr.p2late.p2late.client.terrain

import fr.p2late.p2late.client.graphic.shader.Shader
import fr.p2late.p2late.client.graphic.shader.ShaderProgram
import org.joml.Matrix4f
import org.lwjgl.opengl.GL20

class ChunkShader: ShaderProgram() {

    private val perspectiveMatrixLocation by lazy { getUniformLocation("perspectiveMatrix") }
    private val viewMatrixLocation by lazy { getUniformLocation("viewMatrix") }
    private val lightPositionLocation by lazy { getUniformLocation("lightPosition") }
    private val lightColorLocation by lazy { getUniformLocation("lightColor") }
    private val atlasLocation by lazy { getUniformLocation("atlasSampler") }

    init {
        val vertexShader = Shader.loadShader(GL20.GL_VERTEX_SHADER, "/shaders/chunk.vsh")
        val fragmentShader = Shader.loadShader(GL20.GL_FRAGMENT_SHADER, "/shaders/chunk.fsh")
        attachShader(vertexShader)
        attachShader(fragmentShader)
        link()
        vertexShader.delete()
        fragmentShader.delete()
    }

    fun setPerspectiveMatrix(matrix4f: Matrix4f) {
        setUniform(perspectiveMatrixLocation, matrix4f)
    }

    fun setViewMatrix(matrix4f: Matrix4f) {
        setUniform(viewMatrixLocation, matrix4f)
    }

    fun setLight(lightSource: LightSource){
        setUniform(lightPositionLocation, lightSource.position)
        setUniform(lightColorLocation, lightSource.color)
    }

    fun setAtlas(textureUnit: Int){
        setUniform(atlasLocation, textureUnit)
    }
}