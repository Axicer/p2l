package fr.p2late.p2late.client.terrain

import fr.p2late.p2late.client.graphic.texture.Color
import org.joml.Vector3f

data class LightSource(
    var position: Vector3f,
    var color: Color
)