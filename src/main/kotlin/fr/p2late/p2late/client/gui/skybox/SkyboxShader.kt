package fr.p2late.p2late.client.gui.skybox

import fr.p2late.p2late.client.graphic.shader.Shader
import fr.p2late.p2late.client.graphic.shader.ShaderProgram
import fr.p2late.p2late.client.graphic.texture.Color
import org.joml.Matrix4f
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL32

class SkyboxShader: ShaderProgram() {

    private val perspectiveMatrixLocation by lazy { getUniformLocation("perspectiveMatrix") }
    private val viewMatrixLocation by lazy { getUniformLocation("viewMatrix") }
    private val cubeSizeLocation by lazy { getUniformLocation("cubeSize") }
    private val textureUnitLocation by lazy { getUniformLocation("cubeImage") }
    private val colorLocation by lazy { getUniformLocation("color") }

    init {
        val vertexShader = Shader.loadShader(GL20.GL_VERTEX_SHADER, "/shaders/skybox.vsh")
        val geometryShader = Shader.loadShader(GL32.GL_GEOMETRY_SHADER, "/shaders/skybox.gsh")
        val fragmentShader = Shader.loadShader(GL20.GL_FRAGMENT_SHADER, "/shaders/skybox.fsh")
        attachShader(vertexShader)
        attachShader(geometryShader)
        attachShader(fragmentShader)
        link()
        vertexShader.delete()
        geometryShader.delete()
        fragmentShader.delete()
    }

    fun setPerspectiveMatrix(matrix4f: Matrix4f) {
        setUniform(perspectiveMatrixLocation, matrix4f)
    }

    fun setViewMatrix(matrix4f: Matrix4f) {
        setUniform(viewMatrixLocation, matrix4f)
    }

    fun setCubeSize(value: Float) {
        setUniform(cubeSizeLocation, value)
    }

    fun setTextureUnit(unit: Int) {
        setUniform(textureUnitLocation, unit)
    }

    fun setColor(color: Color){
        setUniform(colorLocation, color)
    }


}