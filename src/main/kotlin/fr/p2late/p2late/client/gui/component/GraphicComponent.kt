package fr.p2late.p2late.client.gui.component

import fr.p2late.p2late.client.P2LClient
import fr.p2late.p2late.client.graphic.GUIModel
import org.joml.Vector2f
import org.joml.Vector3f

abstract class GraphicComponent(
    val model: GUIModel,
    open var position: Vector2f,
    open val rotation: Vector3f,
    open val scale: Vector2f,
    open val anchor: AnchorPoint
) {
    open fun update(delta: Float) {}
    open fun preRender(guiShader: GUIShader) {}
    open fun postRender(guiShader: GUIShader) {}
    open fun additionalRender() {}
    open fun dispose() {}
}

sealed class AnchorPoint(
    open val offsetX: Float, // x offset from bottom left to center
    open val offsetY: Float,  // y offset from bottom left to center
    open val centerOffsetX: Float, // x offset from center to anchor position
    open val centerOffsetY: Float  // y offset from center to anchor position
) {
    object BOTTOM_LEFT : AnchorPoint(0f, 0f, -0.5f, -0.5f)
    object LEFT : AnchorPoint(0f, 0.5f, -0.5f, 0f)
    object TOP_LEFT : AnchorPoint(0f, 1f, -0.5f, 0.5f)
    object BOTTOM : AnchorPoint(0.5f, 0f, 0f, -0.5f)
    object CENTER : AnchorPoint(0.5f, 0.5f, 0f, 0f)
    object TOP : AnchorPoint(0.5f, 1f, 0f, 0.5f)
    object BOTTOM_RIGHT : AnchorPoint(1f, 0f, 0.5f, -0.5f)
    object RIGHT : AnchorPoint(1f, 0.5f, 0.5f, 0f)
    object TOP_RIGHT : AnchorPoint(1f, 1f, 0.5f, 0.5f)
    class Custom(
        override val offsetX: Float,
        override val offsetY: Float,
        override val centerOffsetX: Float,
        override val centerOffsetY: Float
    ) : AnchorPoint(offsetX, offsetY, centerOffsetX, centerOffsetY)

    fun convertBottomLeftCoordinateToAnchorCoordinates(x: Float, y: Float): Pair<Float, Float> {
        val (width, height) = P2LClient.getScreenSize()
        val offX = x + offsetX * width
        val offY = y + offsetY * height
        return offX to offY
    }

    fun convertCenterCoordinateToOppositeAnchorCoordinates(x: Float, y: Float) : Pair<Float, Float>{
        val (width, height) = P2LClient.getScreenSize()
        val offX = x - centerOffsetX * width
        val offY = y - centerOffsetY * height
        return offX to offY
    }

    fun convertCenterCoordinateToAnchorCoordinates(x: Float, y: Float) : Pair<Float, Float>{
        val (width, height) = P2LClient.getScreenSize()
        val offX = x + centerOffsetX * width
        val offY = y + centerOffsetY * height
        return offX to offY
    }
}


