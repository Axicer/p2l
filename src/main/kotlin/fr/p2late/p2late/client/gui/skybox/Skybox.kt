package fr.p2late.p2late.client.gui.skybox

import fr.p2late.p2late.client.P2LClient
import fr.p2late.p2late.client.graphic.Loader
import fr.p2late.p2late.client.graphic.texture.Color
import org.joml.Matrix4f
import org.joml.Vector3f
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL13
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL30

class Skybox(
    val position: Vector3f = Vector3f(0f, 0f, 0f),
    val rotation: Vector3f = Vector3f(0f, 0f, 0f),
    var color: Color = Color(),
    var cubeSize: Float = 1f,
    vararg textures: String = arrayOf(
        "/textures/skybox/left.png",
        "/textures/skybox/right.png",
        "/textures/skybox/bottom.png",
        "/textures/skybox/top.png",
        "/textures/skybox/front.png",
        "/textures/skybox/back.png"
    )
) {
    private val cubeModel = Loader.loadSkybox(*textures)
    val shader = SkyboxShader()

    fun update(delta: Float){
        rotation.y += delta/16
    }

    fun render(){
        shader.start()
        shader.setColor(color)
        shader.setPerspectiveMatrix(P2LClient.camera.perspectiveMatrix)
        shader.setViewMatrix(Matrix4f()
            .rotateX(rotation.x)
            .rotateY(rotation.y)
            .rotateZ(rotation.z)
            .translate(position)
        )
        shader.setCubeSize(cubeSize)
        shader.setTextureUnit(0)
        GL13.glActiveTexture(GL13.GL_TEXTURE0)
        GL11.glBindTexture(GL13.GL_TEXTURE_CUBE_MAP, cubeModel.material.id)

        GL30.glBindVertexArray(cubeModel.rawModel.vaoID)
        GL20.glEnableVertexAttribArray(0)
        GL11.glDrawArrays(GL11.GL_POINTS, 0, cubeModel.rawModel.vaoID)
        GL20.glDisableVertexAttribArray(0)
        GL30.glBindVertexArray(0)

        shader.stop()
    }

    fun dispose(){
        GL11.glDeleteTextures(cubeModel.material.id)
    }
}