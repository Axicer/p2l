package fr.p2late.p2late.client.gui

import fr.p2late.p2late.client.graphic.texture.Color
import fr.p2late.p2late.client.gui.component.AnchorPoint
import fr.p2late.p2late.client.gui.component.ButtonComponent
import fr.p2late.p2late.client.gui.component.GraphicComponent
import fr.p2late.p2late.client.gui.component.ImageComponent
import fr.p2late.p2late.client.gui.skybox.Skybox
import org.joml.Vector2f
import org.joml.Vector3f

object TitleScreenGUI : GUI {

    const val guiName = "TitleScreenGUI"
    private var title: ImageComponent
    private var singlePlayerButton: ButtonComponent
    private var multiPlayerButton: ButtonComponent
    private var settingsButton: ButtonComponent
    private var skybox: Skybox

    init {
        title = ImageComponent(
            "/textures/minecraft.png",
            Color(),
            position = Vector2f(0f, -75f),
            rotation = Vector3f(0f, 0f, 0f),
            scale = Vector2f(600f, 110f),
            anchor = AnchorPoint.TOP
        )
        singlePlayerButton = ButtonComponent(
            "Singleplayer",
            Color(0, 255, 0),
            position = Vector2f(0f, 50f),
            rotation = Vector3f(0f, 0f, 0f),
            scale = Vector2f(250f, 40f),
            anchor = AnchorPoint.CENTER
        )
        multiPlayerButton = ButtonComponent(
            "Multiplayer",
            Color(255, 0, 0),
            position = Vector2f(0f, 0f),
            rotation = Vector3f(0f, 0f, 0f),
            scale = Vector2f(250f, 40f),
            anchor = AnchorPoint.CENTER
        )
        settingsButton = ButtonComponent(
            "Settings",
            Color(0, 0, 255),
            position = Vector2f(0f, -50f),
            rotation = Vector3f(0f, 0f, 0f),
            scale = Vector2f(250f, 40f),
            anchor = AnchorPoint.CENTER
        )
        skybox = Skybox()
    }

    override fun preRender() {
        skybox.render()
    }

    override fun getComponents(): List<GraphicComponent> {
        return mutableListOf(title, singlePlayerButton, multiPlayerButton, settingsButton)
    }

    override fun update(delta: Float) {
        title.update(delta)
        singlePlayerButton.update(delta)
        multiPlayerButton.update(delta)
        settingsButton.update(delta)
        skybox.update(delta)
    }

    override fun dispose() {
        title.dispose()
        singlePlayerButton.dispose()
        multiPlayerButton.dispose()
        settingsButton.dispose()
        skybox.dispose()
    }

    override fun getGUIName() = guiName

}