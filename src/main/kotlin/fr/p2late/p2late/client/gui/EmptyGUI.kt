package fr.p2late.p2late.client.gui

import fr.p2late.p2late.client.gui.component.GraphicComponent

object EmptyGUI : GUI{

    const val guiName = "emptyGUI"

    override fun getComponents(): List<GraphicComponent> {
        return emptyList()
    }

    override fun update(delta: Float) {}
    override fun dispose() {}

    override fun getGUIName() = guiName
}