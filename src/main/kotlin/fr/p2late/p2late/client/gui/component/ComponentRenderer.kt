package fr.p2late.p2late.client.gui.component

import fr.p2late.p2late.client.P2LClient
import org.joml.Matrix4f
import org.joml.Vector2f
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL20
import org.lwjgl.opengl.GL30

class ComponentRenderer(
    private val shader: GUIShader,
    var screenRatio: Vector2f = Vector2f(1f, 1f)
) {
    fun render(components: List<GraphicComponent>) {
        val (width, height) = P2LClient.getScreenSize()
        shader.setPerspectiveMatrix(Matrix4f().ortho2D(0f, width.toFloat(), 0f, height.toFloat()))
        for (component in components) {
            prepareGUIModel(component)
            GL11.glEnable(GL11.GL_BLEND)
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA)
            GL11.glDisable(GL11.GL_DEPTH_TEST)

            shader.setModelMatrix(computeModelMatrix(component))
            component.preRender(shader)
            GL11.glDrawArrays(GL30.GL_TRIANGLE_STRIP, 0, component.model.vertexCount)
            component.postRender(shader)

            GL11.glDisable(GL11.GL_BLEND)
            unbindTexturedModel()
        }
    }

    fun additionalRender(components: List<GraphicComponent>){
        for(component in components){
            component.additionalRender()
        }
    }

    private fun prepareGUIModel(component: GraphicComponent) {
        val rawModel = component.model
        GL30.glBindVertexArray(rawModel.vaoID)
        GL20.glEnableVertexAttribArray(0)
    }

    private fun unbindTexturedModel() {
        GL20.glDisableVertexAttribArray(0)
        GL30.glBindVertexArray(0)
    }

    private fun computeModelMatrix(component: GraphicComponent): Matrix4f {
        val (x, y) = component.anchor.convertBottomLeftCoordinateToAnchorCoordinates(
            component.position.x,
            component.position.y
        )
        return Matrix4f()
            .translate(x, y, 0f)
            .rotateX(component.rotation.x)
            .rotateY(component.rotation.y)
            .rotateZ(component.rotation.z)
            .scale(component.scale.x / 2, component.scale.y / 2, 0f)
    }
}