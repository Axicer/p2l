package fr.p2late.p2late.client.gui.component

import fr.p2late.p2late.client.P2LClient
import fr.p2late.p2late.client.graphic.Loader
import fr.p2late.p2late.client.graphic.text.TextRenderer
import fr.p2late.p2late.client.graphic.texture.Color
import fr.p2late.p2late.client.graphic.texture.Material
import org.joml.Vector2f
import org.joml.Vector3f
import org.lwjgl.glfw.GLFW
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL13

class ButtonComponent(
    private var text: String,
    private var color: Color,
    private val leftAction: () -> Unit = {},
    private val rightAction: () -> Unit = {},
    private val middleAction: () -> Unit = {},
    private val deltaBetweenAction: Int = 0,
    private var enabled: Boolean = true,
    private val textureSrc: String = "/textures/button/default.png",
    private val focusedTextureSrc: String = "/textures/button/focused.png",
    private val clickedTextureSrc: String = "/textures/button/clicked.png",
    private val disabledTextureSrc: String = "/textures/button/clicked.png",
    override var position: Vector2f,
    override val rotation: Vector3f,
    override val scale: Vector2f,
    override val anchor: AnchorPoint
) : GraphicComponent(
    Loader.loadGUI(floatArrayOf(-1f, 1f, -1f, -1f, 1f, 1f, 1f, -1f)), //triangle strip drawing
    position, rotation, scale, anchor
) {

    private var defaultTexture = Material.loadMaterial(textureSrc, 0.0f, 0.0f)
    private var focusedTexture = Material.loadMaterial(focusedTextureSrc, 0.0f, 0.0f)
    private var clickedTexture = Material.loadMaterial(clickedTextureSrc, 0.0f, 0.0f)
    private var disabledTexture = Material.loadMaterial(disabledTextureSrc, 0.0f, 0.0f)
    private var lastActionTime = System.currentTimeMillis()
    private var lastLeftClickState = GLFW.GLFW_RELEASE
    private var lastRightClickState = GLFW.GLFW_RELEASE
    private var lastMiddleClickState = GLFW.GLFW_RELEASE
    private var focused = false
    private var clicked = false

    override fun update(delta: Float) {
        if (!enabled) return

        val window = GLFW.glfwGetCurrentContext()
        val (screenWidth, screenHeight) = P2LClient.getScreenSize()
        val (posX, posY) = P2LClient.getCursorPos().run { first to screenHeight - second }

        val leftClickState = GLFW.glfwGetMouseButton(window, GLFW.GLFW_MOUSE_BUTTON_LEFT)
        val rightClickState = GLFW.glfwGetMouseButton(window, GLFW.GLFW_MOUSE_BUTTON_RIGHT)
        val middleClickState = GLFW.glfwGetMouseButton(window, GLFW.GLFW_MOUSE_BUTTON_MIDDLE)

        //transform cursor coordinate system to GUI coordinate system
        val (cX, cY) = anchor.convertCenterCoordinateToOppositeAnchorCoordinates(
            posX.toFloat() - screenWidth / 2,
            posY.toFloat() - screenHeight / 2
        )
        val bX1 = position.x - scale.x / 2
        val bX2 = position.x + scale.x / 2
        val bY1 = position.y - scale.y / 2
        val bY2 = position.y + scale.y / 2

        if (cX in bX1..bX2 && cY in bY1..bY2) {
            val currentTime = System.currentTimeMillis()
            if (leftClickState == GLFW.GLFW_PRESS) {
                if (currentTime - lastActionTime >= deltaBetweenAction && lastLeftClickState == GLFW.GLFW_RELEASE) {
                    clicked = true
                    lastActionTime = currentTime
                    leftAction.invoke()
                }
            } else if (rightClickState == GLFW.GLFW_PRESS) {
                clicked = true
                if (currentTime - lastActionTime >= deltaBetweenAction && lastRightClickState == GLFW.GLFW_RELEASE) {
                    lastActionTime = currentTime
                    rightAction.invoke()
                }
            } else if (middleClickState == GLFW.GLFW_PRESS) {
                clicked = true
                if (currentTime - lastActionTime >= deltaBetweenAction && lastMiddleClickState == GLFW.GLFW_RELEASE) {
                    lastActionTime = currentTime
                    middleAction.invoke()
                }
            } else {
                clicked = false
            }
            focused = true
        } else {
            clicked = false
            focused = false
        }

        lastLeftClickState = leftClickState
        lastRightClickState = rightClickState
        lastMiddleClickState = middleClickState
    }

    override fun preRender(guiShader: GUIShader) {
        val texture =
            if (!enabled) disabledTexture
            else if (clicked) clickedTexture
            else if (focused) focusedTexture
            else defaultTexture

        guiShader.setTextureUnit(0)
        GL13.glActiveTexture(GL13.GL_TEXTURE0)
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, texture.textureID)
    }

    override fun postRender(guiShader: GUIShader) {
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0)
    }

    override fun additionalRender() {
        TextRenderer.drawText(text, position.x, position.y, color, anchor)
    }

    override fun dispose() {
        GL11.glDeleteTextures(defaultTexture.textureID)
        GL11.glDeleteTextures(focusedTexture.textureID)
        GL11.glDeleteTextures(clickedTexture.textureID)
        GL11.glDeleteTextures(disabledTexture.textureID)
    }
}