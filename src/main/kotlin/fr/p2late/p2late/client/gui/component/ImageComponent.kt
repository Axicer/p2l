package fr.p2late.p2late.client.gui.component

import fr.p2late.p2late.client.graphic.Loader
import fr.p2late.p2late.client.graphic.texture.Color
import fr.p2late.p2late.client.graphic.texture.Material
import org.joml.Vector2f
import org.joml.Vector3f
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL13

class ImageComponent(
    src: String,
    var color: Color = Color(),
    override var position: Vector2f,
    override val rotation: Vector3f,
    override val scale: Vector2f,
    override val anchor: AnchorPoint
) : GraphicComponent(
    Loader.loadGUI(floatArrayOf(-1f, 1f, -1f, -1f, 1f, 1f, 1f, -1f )), //triangle strip drawing
    position, rotation, scale, anchor
) {
    private val material = Material.loadMaterial(src, 0.0f, 0.0f)

    override fun preRender(guiShader: GUIShader) {
        guiShader.setColor(color)
        guiShader.setTextureUnit(0)
        GL13.glActiveTexture(GL13.GL_TEXTURE0)
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, material.textureID)
    }

    override fun postRender(guiShader: GUIShader) {
        guiShader.setColor(Color())
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, 0)
    }

    override fun dispose() {
        GL11.glDeleteTextures(material.textureID)
    }
}