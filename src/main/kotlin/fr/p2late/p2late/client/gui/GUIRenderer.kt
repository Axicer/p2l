package fr.p2late.p2late.client.gui

import fr.p2late.p2late.client.gui.component.ComponentRenderer
import fr.p2late.p2late.client.gui.component.GUIShader
import fr.p2late.p2late.client.gui.component.GraphicComponent

interface GUI{
    fun preRender() {}
    fun getComponents(): List<GraphicComponent>
    fun postRender() {}
    fun update(delta: Float)
    fun getGUIName(): String

    fun dispose()
}

object GUIRenderer {

    private val guiShader = GUIShader()
    private val componentRenderer = ComponentRenderer(guiShader)

    private var guis : MutableList<GUI> = mutableListOf()
    private var currentGUI: GUI = EmptyGUI
    private var lastGUI: GUI? = null
    init {
        guis.add(EmptyGUI)
        guis.add(TitleScreenGUI)
    }

    fun registerGUI(gui: GUI, overwrite: Boolean = false){
        val searchResult = search(gui)
        if(searchResult != null){
            if(overwrite){
                guis.remove(searchResult)
                guis.add(gui)
            }else throw IllegalStateException("GUI ${gui.getGUIName()} is already registered !")
        }else{
            guis.add(gui)
        }
    }

    private fun search(guiName: String) : GUI? = guis.find { it.getGUIName() == guiName }
    private fun search(gui: GUI) : GUI? = search(gui.getGUIName())

    private fun show(guiName: String){
        if(currentGUI.getGUIName() == guiName)return
        val searchResult = search(guiName)
        check(searchResult != null) { "GUI $guiName is not registered !" }
        lastGUI = currentGUI
        currentGUI = searchResult
    }
    fun show(gui: GUIEnum) = show(gui.guiName)

    fun back(resetLastGUI: Boolean = false){
        if(lastGUI != null){
            show(lastGUI!!.getGUIName())
            if(resetLastGUI){
                lastGUI = null
            }
        }
    }

    fun update(delta: Float){
        currentGUI.update(delta)
    }

    fun render(){
        currentGUI.preRender()
        guiShader.start()
        componentRenderer.render(currentGUI.getComponents())
        guiShader.stop()
        componentRenderer.additionalRender(currentGUI.getComponents())
        currentGUI.postRender()
    }

    fun dispose(){
        for(gui in guis){
            gui.dispose()
        }
    }

    fun updateScreenRatio(deltaX: Float, deltaY: Float){
        componentRenderer.screenRatio.x *= deltaX
        componentRenderer.screenRatio.y *= deltaY
    }
}

enum class GUIEnum(val guiName: String){
    TITLE_SCREEN(TitleScreenGUI.guiName),
    EMPTY(EmptyGUI.guiName);
}