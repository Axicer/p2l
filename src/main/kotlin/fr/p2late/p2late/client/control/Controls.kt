package fr.p2late.p2late.client.control

import fr.p2late.p2late.client.gui.GUIRenderer
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.glfw.GLFWKeyCallbackI

object Controls : GLFWKeyCallbackI {

    var isShiftDown = false
    var isControlDown = false

    override fun invoke(window: Long, key: Int, scancode: Int, action: Int, mods: Int) {
        if (action == GLFW_RELEASE && key == GLFW_KEY_ESCAPE) {
            GUIRenderer.back(true)
        }
    }

    fun input(delta: Double) {
        isShiftDown = glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS
        isControlDown = glfwGetKey(glfwGetCurrentContext(), GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS
    }

}