package fr.p2late.p2late.client

import fr.p2late.p2late.client.control.Controls
import fr.p2late.p2late.client.entity.Entity
import fr.p2late.p2late.client.entity.EntityMasterRenderer
import fr.p2late.p2late.client.graphic.text.TextRenderer
import fr.p2late.p2late.client.graphic.texture.Color
import fr.p2late.p2late.client.graphic.texture.TerrainTextureAtlas
import fr.p2late.p2late.client.gui.GUIEnum
import fr.p2late.p2late.client.gui.GUIRenderer
import fr.p2late.p2late.client.gui.component.AnchorPoint
import fr.p2late.p2late.client.terrain.LightSource
import fr.p2late.p2late.client.terrain.WorldRenderer
import fr.p2late.p2late.common.world.World
import org.joml.Vector3f
import org.lwjgl.Version
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.glfw.GLFWErrorCallback
import org.lwjgl.opengl.GL11
import org.lwjgl.system.MemoryStack

object P2LClient {

    private const val TARGET_FPS = 1200000.0
    private lateinit var frame: P2LFrame
    private var running = false

    lateinit var camera: Camera
    private val lightSource = LightSource(
        Vector3f(400f, 1000f, 400f),
        Color()
    )
    private val entities = mutableListOf<Entity>()
    private lateinit var worldRenderer: WorldRenderer

    fun start() {
        init()
        loop()
        dispose()
    }

    private fun init() {
        println("Using LWJGL " + Version.getVersion() + "!")
        GLFWErrorCallback.createPrint(System.err).set()
        if (!glfwInit()) throw IllegalStateException("Unable to initialize GLFW")
        frame = P2LFrame(960, 540, resizable = true, vsync = false)
        camera = Camera(
            Vector3f(0f, 0f, 5f),
            Vector3f(0f, 0f, 0f),
            0.1f,
            1000f,
            Math.toRadians(90.0).toFloat(),
        )
        GUIRenderer.show(GUIEnum.EMPTY)
        TerrainTextureAtlas.createAtlas()

//        val stallModel = Loader.loadOBJFile("/models/stall.obj")
//        val stallMaterial = Material.loadMaterial("/models/stallTexture.png", 0.5f, 10f)
//        val stall = Entity(MaterialModel(stallModel, stallMaterial), Vector3f(0f, 10f, 0f), Vector3f(), 1f)
//        entities.add(stall)

        val world = World()
        world.generateChunk(0,0)
//        val t = 5
//        println("generating terrain...")
//        for (x in -t until t) {
//            for (z in -t until t) {
//                world.generateChunk(x, z)
//            }
//        }
//        println("generation done !")
        worldRenderer = WorldRenderer(world, camera)

        running = true
    }

    private fun loop() {
        var time: Double
        var secondAccumulator = 0.0
        var delta: Double
        var lastTime = glfwGetTime()
        var renderTimeAccumulator = 0.0
        var fps = 0
        var lastFPS = TARGET_FPS.toInt()

        var endTime: Double
        val targetFPSInterval = 1.0 / TARGET_FPS

        while (running) {
            /* Check if game should close */
            if (frame.isClosing()) {
                running = false
            }

            time = glfwGetTime()
            delta = time - lastTime
            secondAccumulator += delta
            renderTimeAccumulator += delta
            lastTime = time

            //update things based on elapsed time between each loops
            Controls.input(delta)
            camera.update(delta.toFloat())

//            entities[0].rotation.y += delta.toFloat()
            GUIRenderer.update(delta.toFloat())
            render()

            val text = "$lastFPS FPS"
            val (width, height) = TextRenderer.getTextSize(text)
            TextRenderer.drawText(
                text,
                width.toFloat() / 2f,
                -height.toFloat() / 2f,
                Color(),
                AnchorPoint.TOP_LEFT
            )
            val pos = "${camera.position.x}, ${camera.position.y}, ${camera.position.z}"
            val (posWidth, posHeight) = TextRenderer.getTextSize(pos)
            TextRenderer.drawText(
                pos,
                posWidth.toFloat() / 2f,
                -posHeight.toFloat() / 2f - height,
                Color(),
                AnchorPoint.TOP_LEFT
            )

            fps++

            while (secondAccumulator >= 1.0) {
                lastFPS = fps
                fps = 0
                secondAccumulator -= 1.0
            }

            glfwSwapBuffers(glfwGetCurrentContext())
            glfwPollEvents()

            endTime = glfwGetTime()
            if (!frame.vsync) {
                //wait until correct framerate delay
                while (endTime - time < targetFPSInterval) {
                    Thread.yield()
                    // avoid consuming CPU but losing some accuracy because
                    // Thread.sleep(1) could sleep longer than 1 millisecond
                    Thread.sleep(1)
                    endTime = glfwGetTime()
                }
            }
        }
    }

    private fun render() {
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT or GL11.GL_DEPTH_BUFFER_BIT)
        GL11.glClearColor(0f, 0f, 0f, 1f)
        worldRenderer.render(lightSource)
        for (entity in entities) {
            EntityMasterRenderer.processEntity(entity)
        }
        EntityMasterRenderer.render(lightSource, camera)
        GUIRenderer.render()
    }

    private fun dispose() {
        worldRenderer.dispose()
        EntityMasterRenderer.dispose()
        GUIRenderer.dispose()
        TextRenderer.dispose()
        TerrainTextureAtlas.dispose()
        frame.destroy()
        glfwTerminate()
    }

    fun getScreenSize(): Pair<Int, Int> {
        val window = glfwGetCurrentContext()
        var width: Int
        var height: Int
        MemoryStack.stackPush().use { stack ->
            val widthBuffer = stack.mallocInt(1)
            val heightBuffer = stack.mallocInt(1)
            glfwGetFramebufferSize(window, widthBuffer, heightBuffer)
            width = widthBuffer.get()
            height = heightBuffer.get()
        }
        return width to height
    }

    fun getCursorPos(): Pair<Int, Int> {
        val window = glfwGetCurrentContext()
        var posX: Int
        var posY: Int
        MemoryStack.stackPush().use { stack ->
            val posXBuffer = stack.mallocDouble(1)
            val posYBuffer = stack.mallocDouble(1)
            glfwGetCursorPos(window, posXBuffer, posYBuffer)
            posX = posXBuffer.get().toInt()
            posY = posYBuffer.get().toInt()
        }
        return posX to posY
    }
}